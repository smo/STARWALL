! ----------------------------------------------------------------------
      subroutine read_coil_data
! ----------------------------------------------------------------------
!
!     purpose:
!
!
! ----------------------------------------------------------------------
       use  coil2d
       use  mpi_v
! ----------------------------------------------------------------------
      implicit none
      include "mpif.h"
      real   ,dimension(:      ),allocatable :: rco,zco,dr,dz,current
      real   ,dimension(:      ),allocatable :: cur_fila
      real   ,dimension(:,:    ),allocatable :: xa,ya,za,xc,yc,zc,phic
      real   ,dimension(:,:,:,:),allocatable :: x_fila,y_fila,z_fila
      integer,dimension(:      ),allocatable :: fila
      integer                          :: i,j,j1,k,l,nel,ne2,nu_fila
      integer                          :: nd_fila
      real                             :: pi2,alv,r1,r2,z1,z2,si,co
      character(80)                    :: txt
! ----------------------------------------------------------------------

!     read coil data
!
  1000 format(i6,1p5e12.4,i6)

  if(rank==0) then  
       open(unit=60,file='coil.txt',status='old')
            read(60,*) txt 
            read(60,*) txt 
            read(60,*) ncoil,nel
            read(60,*) txt 
            read(60,*) txt 
  endif
  !==================================================================
   call MPI_BCAST(ncoil,  1, MPI_INTEGER,    0, MPI_COMM_WORLD, ier)
   call MPI_BCAST(nel,    1, MPI_INTEGER,    0, MPI_COMM_WORLD, ier)
  !==================================================================

   allocate(  rco(ncoil),  zco(ncoil),dr(ncoil),dz(ncoil)  & 
             ,current(ncoil),fila(ncoil),  cur_fila(ncoil)  ) 
   rco=0.; zco=0.; dr=0.; dz=0.; current=0.; fila=0; cur_fila=0.


    if(rank==0) then
         do i=1,ncoil
           read(60,*) rco(i),zco(i),dr(i),dz(i),current(i),fila(i)
           write(6,1000) i,rco(i),zco(i),dr(i),dz(i),current(i),fila(i)
         enddo
      close(60)
   endif
   
!===============================================================================
   call MPI_BCAST(rco,      ncoil, MPI_DOUBLE_PRECISION,  0, MPI_COMM_WORLD, ier)
   call MPI_BCAST(zco,      ncoil, MPI_DOUBLE_PRECISION,  0, MPI_COMM_WORLD, ier)
   call MPI_BCAST(dr,       ncoil, MPI_DOUBLE_PRECISION,  0, MPI_COMM_WORLD, ier)
   call MPI_BCAST(dz,       ncoil, MPI_DOUBLE_PRECISION,  0, MPI_COMM_WORLD, ier)
   call MPI_BCAST(current,  ncoil, MPI_DOUBLE_PRECISION,  0, MPI_COMM_WORLD, ier)
   call MPI_BCAST(fila,     ncoil, MPI_INTEGER,           0, MPI_COMM_WORLD, ier)
!===============================================================================
 
! ----------------------------------------------------------------------

       pi2     = 4.*asin(1.)
       alv     = pi2/float(nel-1)
       ne2     = 2*(nel-1)
       nu_fila = sum(fila) 
       nd_fila = maxval(fila)
       ntri_c = ne2*nu_fila
 
  if(rank==0)    write(6,*) 'coildata',ncoil,nu_fila,nd_fila,nel
  allocate(x_coil(ne2*nu_fila,3),  y_coil(ne2*nu_fila,3)               &
          ,z_coil(ne2*nu_fila,3),phi_coil(ne2*nu_fila,3)               &
          ,jtri_c(ncoil)                                               ) 

  allocate(x_fila(ncoil,nd_fila,nel,2),y_fila(ncoil,nd_fila,nel,2)     &
          ,z_fila(ncoil,nd_fila,nel,2)                                 )
      do i=1,ncoil
        cur_fila(i) = current(i)/float(fila(i))  
          jtri_c(i) = ne2*fila(i)
      enddo
      do i=1,ncoil
       do k=1,fila(i)
           r1 = rco(i)
           r2 = rco(i)
           z1 = zco(i)+.5*dz(i) -(k-1)*dz(i)/float(fila(i))
           z2 = zco(i)+.5*dz(i) -(k  )*dz(i)/float(fila(i))
        do j=0,nel-1
                          co = cos(alv*j)
                          si = sin(alv*j)
           x_fila(i,k,j+1,1) = r1*co
           y_fila(i,k,j+1,1) = r1*si
           z_fila(i,k,j+1,1) = z1
           x_fila(i,k,j+1,2) = r2*co
           y_fila(i,k,j+1,2) = r2*si
           z_fila(i,k,j+1,2) = z2
        enddo
       enddo
      enddo
! ----------------------------------------------------------------------
    allocate(xc(ne2,3),yc(ne2,3),zc(ne2,3),phic(ne2,3)                 &
            ,xa(nel,2),ya(nel,2),za(nel,2)                             )
          j1 = 0
      do i=1,ncoil
       do k=1,fila(i)
          
          xc =0. ; yc =0. ; zc = 0.
        do j=1,nel
           do l=1,2
         xa(j,l) = x_fila(i,k,j,l)  
         ya(j,l) = y_fila(i,k,j,l)  
         za(j,l) = z_fila(i,k,j,l)  
         enddo
        enddo
         call coil_2d(xa,ya,za,nel,cur_fila(i),xc,yc,zc,phic)

        do j=1,ne2
                  j1= j1+1
           x_coil(j1,:) =   xc(j,:) 
           y_coil(j1,:) =   yc(j,:) 
           z_coil(j1,:) =   zc(j,:) 
         phi_coil(j1,:) = phic(j,:) 
        enddo

       enddo
      enddo

    if(rank==0)  write(6,*) 'read_coil_data done',j1, nu_fila*ne2
      end subroutine  read_coil_data
