module resistive

   implicit none
   real,dimension(:,:),allocatable ::  S_ww_loc, a_ey_loc, a_ye_loc, d_ee_loc, s_ww_inv_loc
   real,dimension(  :),allocatable ::  gamma

   integer :: n_w
   integer                         :: n_tor_jorek, i_tor_jorek(999)
   real,    allocatable            :: xyzpot_w(:,:)
   character(len=64)               :: format_type
   character(len=512)              :: char512

end module resistive
