!DIR$ ATTRIBUTES FORCEINLINE :: tri_induct_1
subroutine tri_induct_1(i,xp,yp,zp,ntri_p)

    use tri ! new module with arrays and variables for tri_induct subroutine
    
    implicit none

    integer                   :: ntri_p
    integer                   :: i,i1
    real                      :: x31, y31,z31, x21, y21, z21
    real,dimension (ntri_p,3) :: xp,yp,zp

      i1  = 7*(i-1)
      x21      = xp(i,2) - xp(i,1)
      y21      = yp(i,2) - yp(i,1)
      z21      = zp(i,2) - zp(i,1)
      x31      = xp(i,3) - xp(i,1)
      y31      = yp(i,3) - yp(i,1)
      z31      = zp(i,3) - zp(i,1)
      xm(1+i1) = xp(i,1)+f1*(x21+x31)
      ym(1+i1) = yp(i,1)+f1*(y21+y31)
      zm(1+i1) = zp(i,1)+f1*(z21+z31)
      xm(2+i1) = xp(i,1)+f2*(x21+x31)
      ym(2+i1) = yp(i,1)+f2*(y21+y31)
      zm(2+i1) = zp(i,1)+f2*(z21+z31)
      xm(5+i1) = xp(i,1)+f5*(x21+x31)
      ym(5+i1) = yp(i,1)+f5*(y21+y31)
      zm(5+i1) = zp(i,1)+f5*(z21+z31)
      xm(3+i1) = xp(i,1)+f3*x21+f2*x31
      ym(3+i1) = yp(i,1)+f3*y21+f2*y31
      zm(3+i1) = zp(i,1)+f3*z21+f2*z31
      xm(4+i1) = xp(i,1)+f2*x21+f3*x31
      ym(4+i1) = yp(i,1)+f2*y21+f3*y31
      zm(4+i1) = zp(i,1)+f2*z21+f3*z31
      xm(6+i1) = xp(i,1)+f4*x21+f5*x31
      ym(6+i1) = yp(i,1)+f4*y21+f5*y31
      zm(6+i1) = zp(i,1)+f4*z21+f5*z31
      xm(7+i1) = xp(i,1)+f5*x21+f4*x31
      ym(7+i1) = yp(i,1)+f5*y21+f4*y31
      zm(7+i1) = zp(i,1)+f5*z21+f4*z31

end subroutine  tri_induct_1

!DIR$ ATTRIBUTES FORCEINLINE :: tri_induct_1_b
subroutine tri_induct_1_b(i,xp,yp,zp,ntri_p)

    use tri_b ! new module with arrays and variables for tri_induct subroutine
    
    implicit none

    integer                   :: ntri_p
    integer                   :: i,i1
    real                      :: x31, y31,z31, x21, y21, z21
    real,dimension (ntri_p,3) :: xp,yp,zp

      i1  = 7*(i-1)
      x21      = xp(i,2) - xp(i,1)
      y21      = yp(i,2) - yp(i,1)
      z21      = zp(i,2) - zp(i,1)
      x31      = xp(i,3) - xp(i,1)
      y31      = yp(i,3) - yp(i,1)
      z31      = zp(i,3) - zp(i,1)
      xm_b(1+i1) = xp(i,1)+f1*(x21+x31)
      ym_b(1+i1) = yp(i,1)+f1*(y21+y31)
      zm_b(1+i1) = zp(i,1)+f1*(z21+z31)
      xm_b(2+i1) = xp(i,1)+f2*(x21+x31)
      ym_b(2+i1) = yp(i,1)+f2*(y21+y31)
      zm_b(2+i1) = zp(i,1)+f2*(z21+z31)
      xm_b(5+i1) = xp(i,1)+f5*(x21+x31)
      ym_b(5+i1) = yp(i,1)+f5*(y21+y31)
      zm_b(5+i1) = zp(i,1)+f5*(z21+z31)
      xm_b(3+i1) = xp(i,1)+f3*x21+f2*x31
      ym_b(3+i1) = yp(i,1)+f3*y21+f2*y31
      zm_b(3+i1) = zp(i,1)+f3*z21+f2*z31
      xm_b(4+i1) = xp(i,1)+f2*x21+f3*x31
      ym_b(4+i1) = yp(i,1)+f2*y21+f3*y31
      zm_b(4+i1) = zp(i,1)+f2*z21+f3*z31
      xm_b(6+i1) = xp(i,1)+f4*x21+f5*x31
      ym_b(6+i1) = yp(i,1)+f4*y21+f5*y31
      zm_b(6+i1) = zp(i,1)+f4*z21+f5*z31
      xm_b(7+i1) = xp(i,1)+f5*x21+f4*x31
      ym_b(7+i1) = yp(i,1)+f5*y21+f4*y31
      zm_b(7+i1) = zp(i,1)+f5*z21+f4*z31

end subroutine  tri_induct_1_b


!DIR$ ATTRIBUTES FORCEINLINE :: tri_induct_2
subroutine tri_induct_2(ntri_w,i,xw,yw,zw)

    use tri ! new module with arrays and variables for tri_induct subroutine

    implicit none

    integer                   :: i,ntri_w
    real                      :: x21,y21,z21,x32,y32,z32,x13,y13,z13
    real,dimension (ntri_w,3) :: xw,yw,zw

       x21   = xw(i,2) - xw(i,1)
       y21   = yw(i,2) - yw(i,1)
       z21   = zw(i,2) - zw(i,1)
       x32   = xw(i,3) - xw(i,2)
       y32   = yw(i,3) - yw(i,2)
       z32   = zw(i,3) - zw(i,2)
       x13   = xw(i,1) - xw(i,3)
       y13   = yw(i,1) - yw(i,3)
       z13   = zw(i,1) - zw(i,3)

       d221(i)  = x21**2+y21**2+z21**2
       d232(i)  = x32**2+y32**2+z32**2
       d213(i)  = x13**2+y13**2+z13**2

       nx(i)    = -y21*z13 + z21*y13
       ny(i)    = -z21*x13 + x21*z13
       nz(i)    = -x21*y13 + y21*x13

       area(i) = 1./sqrt(nx(i)*nx(i)+ny(i)*ny(i)+nz(i)*nz(i))

       nx(i)  = nx(i)*area(i)
       ny(i)  = ny(i)*area(i)
       nz(i)  = nz(i)*area(i)
       tx1(i) = y32*nz(i)-z32*ny(i)
       ty1(i) = z32*nx(i)-x32*nz(i)
       tz1(i) = x32*ny(i)-y32*nx(i)
       tx2(i) = y13*nz(i)-z13*ny(i)
       ty2(i) = z13*nx(i)-x13*nz(i)
       tz2(i) = x13*ny(i)-y13*nx(i)
       tx3(i) = y21*nz(i)-z21*ny(i)
       ty3(i) = z21*nx(i)-x21*nz(i)
       tz3(i) = x21*ny(i)-y21*nx(i)

end subroutine  tri_induct_2

!DIR$ ATTRIBUTES FORCEINLINE :: tri_induct_2_b
subroutine tri_induct_2_b(ntri_w,i,xw,yw,zw)

    use tri_b ! new module with arrays and variables for tri_induct subroutine

       implicit none

       integer                   :: i,ntri_w
       real                      :: x21,y21,z21,x32,y32,z32,x13,y13,z13
       real,dimension (ntri_w,3) :: xw,yw,zw

       x21   = xw(i,2) - xw(i,1)
       y21   = yw(i,2) - yw(i,1)
       z21   = zw(i,2) - zw(i,1)
       x32   = xw(i,3) - xw(i,2)
       y32   = yw(i,3) - yw(i,2)
       z32   = zw(i,3) - zw(i,2)
       x13   = xw(i,1) - xw(i,3)
       y13   = yw(i,1) - yw(i,3)
       z13   = zw(i,1) - zw(i,3)

       d221_b(i)  = x21**2+y21**2+z21**2
       d232_b(i)  = x32**2+y32**2+z32**2
       d213_b(i)  = x13**2+y13**2+z13**2

       nx_b(i)    = -y21*z13 + z21*y13
       ny_b(i)    = -z21*x13 + x21*z13
       nz_b(i)    = -x21*y13 + y21*x13

       area_b(i) = 1./sqrt(nx_b(i)*nx_b(i)+ny_b(i)*ny_b(i)+nz_b(i)*nz_b(i))

       nx_b(i)  = nx_b(i)*area_b(i)
       ny_b(i)  = ny_b(i)*area_b(i)
       nz_b(i)  = nz_b(i)*area_b(i)
       tx1_b(i) = y32*nz_b(i)-z32*ny_b(i)
       ty1_b(i) = z32*nx_b(i)-x32*nz_b(i)
       tz1_b(i) = x32*ny_b(i)-y32*nx_b(i)
       tx2_b(i) = y13*nz_b(i)-z13*ny_b(i)
       ty2_b(i) = z13*nx_b(i)-x13*nz_b(i)
       tz2_b(i) = x13*ny_b(i)-y13*nx_b(i)
       tx3_b(i) = y21*nz_b(i)-z21*ny_b(i)
       ty3_b(i) = z21*nx_b(i)-x21*nz_b(i)
       tz3_b(i) = x21*ny_b(i)-y21*nx_b(i)

end subroutine  tri_induct_2_b


!DIR$ ATTRIBUTES FORCEINLINE :: tri_induct_3_2
subroutine tri_induct_3_2(ntri_w,k,i,xw,yw,zw,dima_sca)

    use tri ! new module with arrays and variables for tri_induct subroutine

    implicit none

    integer                   :: ik,k,i,l,ntri_w
    real                      :: x1,y1,z1,x2,y2,z2,x3,y3,z3,s1,s2,s3   &
                              ,al1,al2,al3                             &
                              ,h,s21,s22,s23,dp1,dp2,dp3               &
                              ,ap1,am1,ap2,am2,ap3,am3,ata1,ata2,ata3  &
                              ,ar1,ar2,ar3,ve                          &
                              ,dep1,dep2,dep3,dem1,dem2,dem3

     real,dimension (ntri_w,3)       :: xw,yw,zw
     real :: dima_sca

   do l=1,7

      ik    = l+7*(i-1)
      x1  = xw(k,1) - xm(ik)
      y1  = yw(k,1) - ym(ik)
      z1  = zw(k,1) - zm(ik)
      x2  = xw(k,2) - xm(ik)
      y2  = yw(k,2) - ym(ik)
      z2  = zw(k,2) - zm(ik)
      x3  = xw(k,3) - xm(ik)
      y3  = yw(k,3) - ym(ik)
      z3  = zw(k,3) - zm(ik)
! ----------------------------------------------------------------------
      h   = abs(nx(k)*x1+ny(k)*y1+nz(k)*z1)
      s21 = x1**2+y1**2+z1**2
      s22 = x2**2+y2**2+z2**2
      s23 = x3**2+y3**2+z3**2
      
      s1   = sqrt(s21)
      s2   = sqrt(s22)
      s3   = sqrt(s23)

      !Replaced here arrays d21 onto sqrt(d221(k)) in order to save memory
      al1  = alog((s2+s1+sqrt(d221(k)))/(s2+s1-sqrt(d221(k))))
      al2  = alog((s3+s2+sqrt(d232(k)))/(s3+s2-sqrt(d232(k))))
      al3  = alog((s1+s3+sqrt(d213(k)))/(s1+s3-sqrt(d213(k))))

      ar1   = x1*tx3(k)+y1*ty3(k)+z1*tz3(k)
      ar2   = x2*tx1(k)+y2*ty1(k)+z2*tz1(k)
      ar3   = x3*tx2(k)+y3*ty2(k)+z3*tz2(k)
 
      dp1   = .5*(s22-s21+d221(k))
      dp2   = .5*(s23-s22+d232(k))
      dp3   = .5*(s21-s23+d213(k))

      ap1   = ar1*dp1
      dep1  = ar1**2+h*d221(k)*(h+s2)
      ap2   = ar2*dp2
      dep2  = ar2**2+h*d232(k)*(h+s3)
      ap3   = ar3*dp3
      dep3  = ar3**2+h*d213(k)*(h+s1)
      am1   = ar1*(dp1-d221(k))
      dem1  = ar1**2+h*d221(k)*(h+s1)
      am2   = ar2*(dp2-d232(k))
      dem2  = ar2**2+h*d232(k)*(h+s2)
      am3   = ar3*(dp3-d213(k))
      dem3  = ar3**2+h*d213(k)*(h+s3)

      ata1  = atan2(ap1*dem1-am1*dep1,dep1*dem1+ap1*am1)
      ata2  = atan2(ap2*dem2-am2*dep2,dep2*dem2+ap2*am2)
      ata3  = atan2(ap3*dem3-am3*dep3,dep3*dem3+ap3*am3)

      ve    = pi41*area(k)*(-h*(ata1+ata2+ata3)    &
            + ar1*al1/sqrt(d221(k))+ar2*al2/sqrt(d232(k))+ar3*al3/sqrt(d213(k)))

      dima_sca=dima_sca+w(l)*ve

   enddo
end subroutine  tri_induct_3_2

!DIR$ ATTRIBUTES FORCEINLINE :: tri_induct_3_2_b
subroutine tri_induct_3_2_b(ntri_w,k,i,xw,yw,zw,dima_sca)

    use tri_b ! new module with arrays and variables for tri_induct subroutine

    implicit none

    integer                   :: ik,k,i,l,ntri_w
    real                      :: x1,y1,z1,x2,y2,z2,x3,y3,z3,s1,s2,s3   &
                              ,al1,al2,al3                             &
                              ,h,s21,s22,s23,dp1,dp2,dp3               &
                              ,ap1,am1,ap2,am2,ap3,am3,ata1,ata2,ata3  &
                              ,ar1,ar2,ar3,ve                          &
                              ,dep1,dep2,dep3,dem1,dem2,dem3

     real,dimension (ntri_w,3)       :: xw,yw,zw
     real :: dima_sca

    do l=1,7

      ik    = l+7*(i-1)
      x1  = xw(k,1) - xm_b(ik)
      y1  = yw(k,1) - ym_b(ik)
      z1  = zw(k,1) - zm_b(ik)
      x2  = xw(k,2) - xm_b(ik)
      y2  = yw(k,2) - ym_b(ik)
      z2  = zw(k,2) - zm_b(ik)
      x3  = xw(k,3) - xm_b(ik)
      y3  = yw(k,3) - ym_b(ik)
      z3  = zw(k,3) - zm_b(ik)
! ----------------------------------------------------------------------
      h   = abs(nx_b(k)*x1+ny_b(k)*y1+nz_b(k)*z1)
      s21 = x1**2+y1**2+z1**2
      s22 = x2**2+y2**2+z2**2
      s23 = x3**2+y3**2+z3**2
      s1   = sqrt(s21)
      s2   = sqrt(s22)
      s3   = sqrt(s23)

          !Replaced here arrays d21 onto sqrt(d221(k)) in order to save memory
          al1  = alog((s2+s1+sqrt(d221_b(k)))/(s2+s1-sqrt(d221_b(k))))
          al2  = alog((s3+s2+sqrt(d232_b(k)))/(s3+s2-sqrt(d232_b(k))))
          al3  = alog((s1+s3+sqrt(d213_b(k)))/(s1+s3-sqrt(d213_b(k))))

          ar1   = x1*tx3_b(k)+y1*ty3_b(k)+z1*tz3_b(k)
      ar2   = x2*tx1_b(k)+y2*ty1_b(k)+z2*tz1_b(k)
      ar3   = x3*tx2_b(k)+y3*ty2_b(k)+z3*tz2_b(k)
      dp1   = .5*(s22-s21+d221_b(k))
      dp2   = .5*(s23-s22+d232_b(k))
      dp3   = .5*(s21-s23+d213_b(k))
      ap1   = ar1*dp1
      dep1  = ar1**2+h*d221_b(k)*(h+s2)
      ap2   = ar2*dp2
      dep2  = ar2**2+h*d232_b(k)*(h+s3)
      ap3   = ar3*dp3
      dep3  = ar3**2+h*d213_b(k)*(h+s1)
      am1   = ar1*(dp1-d221_b(k))
      dem1  = ar1**2+h*d221_b(k)*(h+s1)
      am2   = ar2*(dp2-d232_b(k))
      dem2  = ar2**2+h*d232_b(k)*(h+s2)
      am3   = ar3*(dp3-d213_b(k))
      dem3  = ar3**2+h*d213_b(k)*(h+s3)
      ata1  = atan2(ap1*dem1-am1*dep1,dep1*dem1+ap1*am1)
      ata2  = atan2(ap2*dem2-am2*dep2,dep2*dem2+ap2*am2)
      ata3  = atan2(ap3*dem3-am3*dep3,dep3*dem3+ap3*am3)
          ve    = pi41*area_b(k)*(-h*(ata1+ata2+ata3)    &
            + ar1*al1/sqrt(d221_b(k))+ar2*al2/sqrt(d232_b(k))+ar3*al3/sqrt(d213_b(k)))

         dima_sca=dima_sca+w(l)*ve

   enddo
end subroutine  tri_induct_3_2_b
