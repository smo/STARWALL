!----------------------------------------------------------------------
 subroutine matrix_cc 
! ----------------------------------------------------------------------
!   purpose:                                                  13/12/05
! ----------------------------------------------------------------------
      use coil2d
      use solv
      use tri
      use tri_w
      use mpi_v
      use sca
! ----------------------------------------------------------------------
      implicit none
      integer                 :: i,i0,i1,ip,ip0,ip1,j,jp
      real                    :: pi,mu0
      real                    :: dx1,dy1,dz1,dx2,dy2,dz2,dx3,dy3,dz3
      real   ,allocatable     :: jx(:),jy(:),jz(:)
      
      real :: dima_sca,dima_sca2,dima_sum,num

      integer    j_loc,i_loc
      logical    inside_j,inside_i

! ----------------------------------------------------------------------
if (rank==0) write(6,*) 'compute matrix_cc'
! ----------------------------------------------------------------------
if (rank==0) write(6,*) 'ma_cc ntri_c=',ntri_c,'ncoil=',ncoil
 
allocate(jx(ntri_c),jy(ntri_c),jz(ntri_c),stat=ier)
         if (ier /= 0) then
             print *,'matrix_cc.f90: Can not allocate local arrays jx, ..., jz'
             stop
         endif
 
 jx=0.; jy=0.; jz=0. 
 
 allocate(nx(ntri_c),ny(ntri_c),nz(ntri_c),tx1(ntri_c),         &
          ty1(ntri_c),tz1(ntri_c),tx2(ntri_c),ty2(ntri_c),      &
          tz2(ntri_c),tx3(ntri_c),ty3(ntri_c),tz3(ntri_c),      &
          d221(ntri_c),d232(ntri_c),d213(ntri_c),area(ntri_c),  &
          xm(7*ntri_c), ym(7*ntri_c), zm(7*ntri_c),stat=ier)
          if (ier /= 0) then
             print *,'matrix_cc.f90: Can not allocate local arrays nx, ..., zm'
             stop
          endif

nx=0.; ny=0.; nz=0.; tx1=0.; ty1=0.;tz1=0.; tx2=0.
ty2=0.; tz2=0.; tx3=0.; ty3=0.; tz3=0.; d221=0.
d232=0.; d213=0.;area=0.; xm=0.; ym=0.; zm=0.

!-----------------------------------------------------------------------


pi  = 2.*asin(1.)
mu0 = 4.e-07*pi 

      do j=1,ntri_c
         dx1 = x_coil(j,2)-x_coil(j,3)
         dy1 = y_coil(j,2)-y_coil(j,3)
         dz1 = z_coil(j,2)-z_coil(j,3)
         dx2 = x_coil(j,3)-x_coil(j,1)
         dy2 = y_coil(j,3)-y_coil(j,1)
         dz2 = z_coil(j,3)-z_coil(j,1)
         dx3 = x_coil(j,1)-x_coil(j,2)
         dy3 = y_coil(j,1)-y_coil(j,2)
         dz3 = z_coil(j,1)-z_coil(j,2)
         
         jx(j) = dx1*phi_coil(j,1)+dx2*phi_coil(j,2)+dx3*phi_coil(j,3)
         jy(j) = dy1*phi_coil(j,1)+dy2*phi_coil(j,2)+dy3*phi_coil(j,3)
         jz(j) = dz1*phi_coil(j,1)+dz2*phi_coil(j,2)+dz3*phi_coil(j,3)
      
         call tri_induct_1(j,x_coil,y_coil,z_coil,ntri_c)
         call tri_induct_2(ntri_c,j,x_coil,y_coil,z_coil)
     
     enddo
       
      i0 = 0
       do j =1,ncoil
           call ScaLAPACK_mapping_j(j,j_loc,inside_j)
           if (inside_j) then! If not inside

            do i =1,jtri_c(j)
               i1 = i0 +i
               ip0 = 0
               do jp = 1,ncoil
                  call  ScaLAPACK_mapping_i(jp,i_loc,inside_i)
                  if (inside_i) then! If not inside

                    do ip = 1,jtri_c(jp)
                       ip1 = ip0 +ip
                        
                           dima_sca=0
                           dima_sca2=0
                           
                           call tri_induct_3_2(ntri_c,ip1,i1,x_coil,y_coil,z_coil,dima_sca)
                           call tri_induct_3_2(ntri_c,i1,ip1,x_coil,y_coil,z_coil,dima_sca2)
   
                           dima_sum=dima_sca+dima_sca2
                          
  
                           a_ww_loc(i_loc,j_loc) = a_ww_loc(i_loc,j_loc)+&
                                               .5*dima_sum           &
                                                 *(jx(ip1)*jx(i1)    & 
                                                 +jy(ip1)*jy(i1)     &        
                                                 +jz(ip1)*jz(i1) ) 
                   
                     enddo
                  endif
                  ip0 = ip0+jtri_c(jp)
                  
               enddo
             enddo
           endif
            i0 = i0+jtri_c(j)
            
       enddo

      deallocate(jz,jy,jx)
      deallocate(nx,ny,nz,tx1,ty1,tz1,tx2,ty2, &
                 tz2,tx3,ty3,tz3,d221,d232,d213,area,xm,ym,zm)

!-------------------------------------------------------------------

   CALL DESCINIT(DESCA, npot_w+ncoil, npot_w+ncoil, NB, NB, 0, 0, CONTEXT, LDA_ww, INFO_A )
   if(INFO_A .NE. 0) then
          write(6,*) "Something is wrong in matrix_cc  CALL DESCINIT DESCA, INFO_A=",INFO_A
          stop
   endif


   if(rank==0) then
      open(500,file='coil_inductances.txt',status='unknown')
      write(500,*) 'inductances L_ik microHenry'
      write(500,*) '  i   k   L_ik  '
   endif

      do j=1,ncoil
       do jp=1,ncoil
             CALL pdelget('A','D',num, a_ww_loc,j,jp,DESCA)
             if(rank==0)  write(500,5000) j,jp,mu0*1.e+6*num
              5000 format(2i4,1pe12.4)
       enddo
      enddo

   if(rank==0)  close(500)


!-------------------------------------------------------------------
if(rank==0) write(*,*) 'matrix_cc  done'
if(rank==0) write(*,*) '==============================================================='
end subroutine  matrix_cc
