! ----------------------------------------------------------------------
subroutine matrix_ec
! ----------------------------------------------------------------------
!     purpose:                                              17/08/09
!
! ----------------------------------------------------------------------
use icontr
use contr_su
use coil2d
use solv
use mpi_v
! ----------------------------------------------------------------------
implicit none
! ----------------------------------------------------------------------
real,dimension(:,:),allocatable :: b_coil,a_c,a_s,b_c,b_s
real,dimension(:,:),allocatable :: xt,yt,zt,phi
real,dimension(  :),allocatable :: bx,by,bz
real                            :: pi2,alv,fnv
integer                         :: i,ic,ic0,ic1,i_start,k_start
integer                         :: j,jt,k,ku,kv,nuv,m

integer    j_loc,i_loc
logical    inside_j,inside_i
! ----------------------------------------------------------------------

if(rank==0) write(6,*) 'compute matrix_ec'

pi2  = 4.*asin(1.)
fnv   = 1./float(nv)
alv  = pi2*fnv
nuv  = nu*nv

i_start = 1

allocate(b_coil(nuv,   ncoil), bx(nuv),by(nuv),bz(nuv),              & 
         a_c(nu,       ncoil), a_s(nu,     ncoil),                   & 
         b_c(n_dof_bnd,ncoil), b_s(n_dof_bnd,ncoil), stat=ier          )

b_coil=0.; bx=0.; by=0.; bz=0.; a_c=0.; a_s=0.; b_c=0.; b_s=0. 

ic0 = 0

do j=1,ncoil

  jt = jtri_c(j)

  allocate(xt(jt,3),yt(jt,3),zt(jt,3),phi(jt,3))

  do ic=1,jt
    ic1= ic0 + ic
    xt(ic,:) =   x_coil(ic1,:)
    yt(ic,:) =   y_coil(ic1,:)
    zt(ic,:) =   z_coil(ic1,:)
    phi(ic,:) = phi_coil(ic1,:)
  enddo

  b_coil=0.; bx=0.; by=0.; bz=0.

  call bfield_c(x,y,z,bx,by,bz,nuv,xt,yt,zt,phi,jtri_c(j))

  do i=1,nuv
    b_coil(i,j)  =  xu(i)*bx(i)+yu(i)*by(i)+zu(i)*bz(i) 
  enddo

  deallocate(phi,zt,yt,xt)

  ic0 =ic0+jtri_c(j)

enddo


if (n_tor(1).eq.0) then

  a_c = 0.; b_c= 0.

  do  ku=1,nu
    do  kv =1,nv
      j = ku+nu*(kv-1)
      do k=1,ncoil
        a_c(ku,k) = a_c(ku,k) + b_coil(j,k)*fnv
      enddo
    enddo
  enddo

  call real_space2bezier(b_c,a_c,N_bnd,n_points,ncoil,n_bnd_nodes,bnd_node,bnd_node_index,n_dof_bnd,Lij)
 
  do i=1,N_bnd

    j = 1 + (N_harm+Nd_harm0)*(i-1)
    do m=1,ncoil
        call  ScaLAPACK_mapping_i(j,i_loc,inside_i)
        if (inside_i) then! If not inside
               call ScaLAPACK_mapping_j(m,j_loc,inside_j)
               if (inside_j) then! If not inside
                    a_ew_loc_sca(i_loc, j_loc) = b_c(2*i-1,m)
               endif        
        endif
 
        call  ScaLAPACK_mapping_i(j+1,i_loc,inside_i)
        if (inside_i) then! If not inside
               call ScaLAPACK_mapping_j(m,j_loc,inside_j)
               if (inside_j) then! If not inside
                    a_ew_loc_sca(i_loc, j_loc) = b_c(2*i,m)
               endif
        endif

              
    enddo
  enddo

  i_start = 2

endif 

if ((n_tor(1) .ne. 0) .or. (n_harm .gt. 1))   then

  do i=  i_start,n_harm           !   toroidal harmonics

    a_c=0.; a_s=0.; b_c=0.; b_s=0.

    do  ku=1,nu
      do  kv =1,nv
        j = ku+nu*(kv-1)
        do ic =1,ncoil
          a_c(ku,ic) = a_c(ku,ic) + b_coil(j,ic)*cos(alv*n_tor(i)*(kv-1))*fnv*2
          a_s(ku,ic) = a_s(ku,ic) + b_coil(j,ic)*sin(alv*n_tor(i)*(kv-1))*fnv*2
        enddo
      enddo
    enddo

    call real_space2bezier(b_c,a_c,N_bnd,n_points,ncoil,n_bnd_nodes,bnd_node,bnd_node_index,n_dof_bnd,Lij)
    call real_space2bezier(b_s,a_s,N_bnd,n_points,ncoil,n_bnd_nodes,bnd_node,bnd_node_index,n_dof_bnd,Lij)

    do k =1,n_bnd_nodes

     k_start = bnd_node_index(k,1)
     j = 2*i_start-1 +4*(i-i_start) + (nd_harm+nd_harm0)/2 *(k_start-1)

     do ic=1,ncoil
        call  ScaLAPACK_mapping_i(j,i_loc,inside_i)
        if (inside_i) then! If not inside
               call ScaLAPACK_mapping_j(ic,j_loc,inside_j)
               if (inside_j) then! If not inside
                    a_ew_loc_sca(i_loc, j_loc) = b_c(k_start,  ic)
               endif
        endif

        call  ScaLAPACK_mapping_i(j+1,i_loc,inside_i)
        if (inside_i) then! If not inside
               call ScaLAPACK_mapping_j(ic,j_loc,inside_j)
               if (inside_j) then! If not inside
                    a_ew_loc_sca(i_loc, j_loc) = b_c(k_start+1,  ic)
               endif
        endif

        call  ScaLAPACK_mapping_i(j+2,i_loc,inside_i)
        if (inside_i) then! If not inside
               call ScaLAPACK_mapping_j(ic,j_loc,inside_j)
               if (inside_j) then! If not inside
                    a_ew_loc_sca(i_loc, j_loc) = b_s(k_start,  ic)
               endif
        endif


        call  ScaLAPACK_mapping_i(j+3,i_loc,inside_i)
        if (inside_i) then! If not inside
               call ScaLAPACK_mapping_j(ic,j_loc,inside_j)
               if (inside_j) then! If not inside
                    a_ew_loc_sca(i_loc, j_loc) = b_s(k_start+1,  ic)
               endif
        endif
      
      enddo
    enddo

  enddo
endif

deallocate(b_s,b_c,a_s,a_c,bz,by,bx,b_coil)

if(rank==0) write(*,*) 'matrix_ec  done'
if(rank==0) write(*,*) '==============================================================='

end subroutine matrix_ec 
