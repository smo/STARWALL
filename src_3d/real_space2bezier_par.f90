!----------------------------------------------------------------------
subroutine real_space2bezier_par(bez, f, n_bnd_elm, n_points, ndim,    &
                                 n_bnd_nodes, bnd_node, bnd_node_index,& 
                                 n_dof_bnd, Lij, npot_p_loc_b,npot_p_loc_e)
! ----------------------------------------------------------------------
!   purpose:                                                  12/08/09
!
! ----------------------------------------------------------------------
implicit none
integer                                 :: n_bnd_elm, n_points, ndim, n_bnd_nodes, n_dof_bnd
real,dimension(n_dof_bnd,   npot_p_loc_b:npot_p_loc_e)       :: bez 
real,dimension(n_bnd_elm*n_points,npot_p_loc_b:npot_p_loc_e) :: f 
integer,dimension(n_bnd_elm,2)          :: bnd_node
integer,dimension(n_bnd_nodes,2)        :: bnd_node_index(n_bnd_nodes,2)
real,dimension(2,2,n_bnd_elm)           :: Lij
! ----------------------------------------------------------------------
real,dimension(4,4)                     :: LSQ
real,dimension(n_dof_bnd,n_dof_bnd)     :: aa, t        ! dimensions to be adjusted
real,dimension(n_dof_bnd,   ndim)       :: b 
real,dimension(2,2)                     :: H,H_s,H_ss 
real,dimension(n_points)                :: h11,h12,h21,h22
real                                    :: s,fnp
integer                                 :: i,idim,im,im2,ip,ip2,j,k,nd
integer                                 :: i1,i2,i3,i4,info


integer :: npot_p_loc_b,npot_p_loc_e

! ----------------------------------------------------------------------

! coefficient matrix for least-square fit to Bezier functions of one boundary element (see doc for derivation). 

LSQ = reshape([  52., 22., 18.,  13., &
                 22., 12., 13.,   9., &
                 18., 13., 52.,  22., &
                 13.,  9., 22.,  12.] / 140., shape(LSQ))

aa = 0.

do i=1,n_bnd_elm  ! N_bnd: number of boundary elements from JOREK

  i1 = bnd_node_index(bnd_node(i,1),1)
  i2 = bnd_node_index(bnd_node(i,1),2)
  i3 = bnd_node_index(bnd_node(i,2),1)
  i4 = bnd_node_index(bnd_node(i,2),2)

  aa(i1,i1) = aa(i1,i1) + LSQ(1,1) * Lij(1,1,i) * Lij(1,1,i)
  aa(i1,i2) = aa(i1,i2) + LSQ(1,2) * Lij(1,2,i) * Lij(1,1,i)
  aa(i1,i3) = aa(i1,i3) + LSQ(1,3) * Lij(2,1,i) * Lij(1,1,i)
  aa(i1,i4) = aa(i1,i4) + LSQ(1,4) * Lij(2,2,i) * Lij(1,1,i)

  aa(i2,i1) = aa(i2,i1) + LSQ(2,1) * Lij(1,1,i) * Lij(1,2,i)
  aa(i2,i2) = aa(i2,i2) + LSQ(2,2) * Lij(1,2,i) * Lij(1,2,i)
  aa(i2,i3) = aa(i2,i3) + LSQ(2,3) * Lij(2,1,i) * Lij(1,2,i)
  aa(i2,i4) = aa(i2,i4) + LSQ(2,4) * Lij(2,2,i) * Lij(1,2,i)

  aa(i3,i1) = aa(i3,i1) + LSQ(3,1) * Lij(1,1,i) * Lij(2,1,i)
  aa(i3,i2) = aa(i3,i2) + LSQ(3,2) * Lij(1,2,i) * Lij(2,1,i)
  aa(i3,i3) = aa(i3,i3) + LSQ(3,3) * Lij(2,1,i) * Lij(2,1,i)
  aa(i3,i4) = aa(i3,i4) + LSQ(3,4) * Lij(2,2,i) * Lij(2,1,i)

  aa(i4,i1) = aa(i4,i1) + LSQ(4,1) * Lij(1,1,i) * Lij(2,2,i)
  aa(i4,i2) = aa(i4,i2) + LSQ(4,2) * Lij(1,2,i) * Lij(2,2,i)
  aa(i4,i3) = aa(i4,i3) + LSQ(4,3) * Lij(2,1,i) * Lij(2,2,i)
  aa(i4,i4) = aa(i4,i4) + LSQ(4,4) * Lij(2,2,i) * Lij(2,2,i)

enddo

t = 0.
do  i=1,n_dof_bnd
  t(i,i) = 1.
enddo

nd  = n_dof_bnd

call dpotrf('U',nd,aa,nd,info)
call dpotrs('U',nd,nd,aa,nd,t,nd,info)

! ----------------------------------------------------------------------
b   = 0.
fnp = 1./float(n_points)

do j=1,n_points

  s = float(j-1)/float(n_points)

  call basisfunctions1(s,H,H_s,H_ss)

  h11(j) =  H(1,1) * fnp       ! where does fnp come from? (from the integration over n_points?)
  h12(j) =  H(1,2) * fnp
  h21(j) =  H(2,1) * fnp
  h22(j) =  H(2,2) * fnp

enddo

do i = 1, n_bnd_elm

  im  = bnd_node_index(bnd_node(i,1),1)    ! node 1, first dof 
  ip  = bnd_node_index(bnd_node(i,1),2)    ! node 1, second dof
  im2 = bnd_node_index(bnd_node(i,2),1)    ! node 2, first dof
  ip2 = bnd_node_index(bnd_node(i,2),2)    ! node 2, second dof

  do k = 1, n_points

    j = k + n_points*(i-1)

    do idim = npot_p_loc_b,npot_p_loc_e
      b(im, idim) =  b(im, idim) + h11(k) * f(j,idim) * Lij(1,1,i)
      b(ip, idim) =  b(ip, idim) + h12(k) * f(j,idim) * Lij(1,2,i)
      b(im2,idim) =  b(im2,idim) + h21(k) * f(j,idim) * Lij(2,1,i)
      b(ip2,idim) =  b(ip2,idim) + h22(k) * f(j,idim) * Lij(2,2,i)
    enddo

  enddo

enddo

bez = 0.

do i=1,n_dof_bnd
  do k=1,n_dof_bnd
    do idim=npot_p_loc_b,npot_p_loc_e
      bez(i,idim) = bez(i,idim) + t(i,k)*b(k,idim) 
    enddo
  enddo
enddo

end subroutine real_space2bezier_par
