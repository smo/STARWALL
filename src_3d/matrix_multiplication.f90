subroutine matrix_multiplication
use solv
use tri_p
use mpi_v
use sca

!-----------------------------------------------------------------------
implicit none

if(rank==0) write(*,*) "matrix_multiplication starts"


!a_wp
CALL DESCINIT(DESCA,nd_w, npot_p, NB, NB, 0, 0, CONTEXT, LDA_wp, INFO_A )
if(INFO_A .NE. 0) then
  write(6,*) "Something is wrong in matrix_multiplication.f90 CALL DESCINIT DESCA, INFO_A=",INFO_A
  stop
endif

!a_pwe
CALL DESCINIT(DESCB,npot_p, nd_w, NB, NB, 0, 0, CONTEXT, LDA_pwe, INFO_B )
if(INFO_B .NE. 0) then
  write(6,*) "Something is wrong in matrix_multiplication.f90 CALL DESCINIT DESCB, INFO_B=",INFO_B
  stop
endif

!a_ww
CALL DESCINIT(DESCC,nd_w, nd_w, NB, NB, 0, 0, CONTEXT, LDA_ww, INFO_C )
if(INFO_B .NE. 0) then
  write(6,*) "Something is wrong in matrix_multiplication.f90  CALL DESCINIT DESCC, INFO_C=",INFO_C
  stop
endif


CALL PDGEMM('N','N', nd_w ,nd_w, npot_p, -1.0D0, a_wp_loc,1,1,DESCA, a_pwe_loc,1,1,DESCB,&
             1.0D0, a_ww_loc,1 , 1 , DESCC) 


if(rank==0) write(*,*) "matrix_multiplication complited"
if(rank==0) write(*,*) '==============================================================='

end subroutine matrix_multiplication

