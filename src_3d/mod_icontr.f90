module  icontr

   implicit none
   integer,parameter,private :: MAX_N_TOR = 101 ! maximum number of toroidal modes
   integer,parameter         :: MAX_MN_W  = 8 ! maximum number of wall Fourier harmonics
   integer,parameter         :: inp=5, outp=6
   integer                   :: n_harm,nd_harm,nd_harm0
   integer                   :: nv,iwall,nwall,i_response,nu_coil
   real                      :: delta
   integer                   :: n_tor(MAX_N_TOR)

end module icontr
