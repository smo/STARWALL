subroutine computing_s_ww_inverse
use mpi_v
use sca
use resistive
!-----------------------------------------------------------------------
implicit none


integer, allocatable :: ipiv(:)
real,   dimension(:),allocatable   :: WORK
integer,dimension(:),allocatable   :: IWORK
integer (KIND=8) LWORK
integer ::  LIWORK


if(rank==0) write(*,*) "computing_s_ww_inverse starts"

   !Set up scalapack sub grid
   call SCA_GRID(n_w,n_w)
   !allocate local matrix
   allocate(s_ww_inv_loc(MP_A,NQ_A), stat=ier)
   IF (IER /= 0) THEN
             WRITE (*,*) " Can not allocate local matrix s_ww_inv_loc: MY_PROC_NUM=",MYPNUM
             STOP
   END IF

    LDA_s_ww_inv=LDA_A
    s_ww_inv_loc=0.


! --- Determine inverse of S_ww matrix
   allocate( ipiv(n_w),stat=ier )
   IF (IER /= 0) THEN
             WRITE (*,*) " Can not allocate local array ipiv: MY_PROC_NUM=",MYPNUM
             STOP
   END IF

   s_ww_inv_loc(:,:) = s_ww_loc(:,:)

   CALL DESCINIT(DESCA,n_w, n_w, NB, NB, 0, 0, CONTEXT, LDA_s_ww_inv, INFO_A)
   CALL PDGETRF( n_w  , n_w , s_ww_inv_loc , 1 , 1 ,  DESCA , IPIV , INFO )
   if ( INFO /= 0 ) then
       write(*,*) 'ERROR calling PDGETRF  when computing inverse of S_ww_loc. info=', INFO
       stop
   endif

   allocate(WORK(1),IWORK(1), stat=ier)
   IF (IER /= 0) THEN
             WRITE (*,*) " Can not allocate local matrix WORK, IWORK: MY_PROC_NUM=",MYPNUM
             STOP
   END IF

   WORK=0
   IWORK=0
   LWORK=-1
   LIWORK=-1

   !Here we calculate only the workspace
   CALL PDGETRI(  n_w , s_ww_inv_loc , 1 ,  1 ,DESCA ,IPIV, WORK ,  LWORK  , IWORK , LIWORK  , INFO )
   if ( INFO /= 0 ) then
       write(*,*) 'ERROR calling PDGETRI  when computing inverse of S_ww_loc. info=', INFO
       stop
   endif

   lwork =lwork_cooficient*INT(ABS(work(1)))+1
   if(rank==0) write(6,*) "    Need to allocate = ", REAL(lwork*8.0/1024.0/1024.0/1024.0), "GB workspace array"
   DEALLOCATE(work)
   ALLOCATE(work(LWORK),stat=ier)
   IF (IER /= 0) THEN
            WRITE (*,*) "computing_s_ww_inverse: Can not allocate WORK: MY_PROC_NUM=",MYPNUM, "LWORK=",LWORK
            STOP
   END IF

   liwork =lwork_cooficient*INT (ABS(iwork(1)))
   DEALLOCATE(iwork)
   ALLOCATE(iwork(liwork),stat=ier)
   IF (IER /= 0) THEN
            WRITE (*,*) "computing_s_ww_inverse: Can not allocate IWORK: MY_PROC_NUM=",MYPNUM
            STOP
   END IF

   if(rank==0) write(6,*) "    Need to allocate 2 = ", REAL(liwork*8.0/1024.0/1024.0/1024.0), "GB workspace array"

! Here calculation is performed

  CALL PDGETRI(  n_w , s_ww_inv_loc , 1 ,  1 ,DESCA ,IPIV, WORK ,  LWORK  , IWORK , LIWORK  , INFO )
  if ( INFO /= 0 ) then
       write(*,*) 'ERROR calling PDGETRI  when computing inverse of S_ww_loc. info=', INFO
       stop
  endif


  deallocate( ipiv, work, iwork )


if(rank==0) write(*,*) "computing_s_ww_inverse complited"
if(rank==0) write(*,*) '==============================================================='

end subroutine computing_s_ww_inverse

