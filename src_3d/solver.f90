subroutine solver
use icontr
use solv 
use contr_su
use tri_w
use tri_p
use coil2d

use mpi_v

implicit none

if(rank==0) write(*,*) ' SOLVER: n_dof_bnd : ',n_dof_bnd

nd_bez  = (nd_harm+nd_harm0)/2 * n_dof_bnd 
nd_w    = ncoil + npot_w
nd_we   = nd_w + nd_bez

if(rank==0) write(*,*) ' SOLVER: nd_bez : ',nd_bez
if(rank==0) write(*,*) ' SOLVER: nd_w :   ',nd_w
if(rank==0) write(*,*) ' SOLVER: nd_we :  ',nd_we
if(rank==0) write(*,*) ' SOLVER: npot_p : ',npot_p

!==================================================================
call matrix_pp
!==================================================================

!==================================================================
call matrix_wp
!==================================================================

!==================================================================
call matrix_ww
!==================================================================

!==================================================================
call matrix_rw
!==================================================================

!==================================================================
if (nu_coil.ne.0) then
  call matrix_cc
  call matrix_cp
  call matrix_wc
  call matrix_rc
endif
!==================================================================

!==================================================================
call matrix_pe
!==================================================================

!==================================================================
call matrix_ep
!==================================================================

!==================================================================
call matrix_ew
!==================================================================

!==================================================================
if (nu_coil.ne.0) then 
  call matrix_ec ! Here was a bug in sequential version
endif
!==================================================================


if(rank==0) write(*,'(A,i6)') 'SOLVER : npot_p : ',npot_p 
if(rank==0) write(*,'(A,i6)') 'SOLVER : nd_w   : ',nd_w 
if(rank==0) write(*,'(A,2i7,A,2i7)') 'SOLVER : copying a_wp ', nd_w, npot_p,' to a_pwe ',npot_w,nd_we
if(rank==0) write(*,*) '==============================================================='

!==================================================================
call a_pe_transpose_sca
call cholesky_solver
!==================================================================

deallocate(a_pp_loc)

!==================================================================
call a_pwe_s_computing
!==================================================================

!==================================================================
call a_ee_computing
!==================================================================

!==================================================================
call a_ew_computing
!==================================================================

!==================================================================
call a_we_computing
!==================================================================

!==================================================================
call matrix_multiplication
!==================================================================

deallocate(a_wp_loc,a_pwe_loc,a_ep_loc_sca)

end subroutine solver
