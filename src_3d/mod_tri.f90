module tri

        implicit none

	real, dimension(:),allocatable :: nx,ny,nz,&
        tx1,ty1,tz1,tx2,ty2,tz2,tx3,ty3,tz3,       &
        d221,d232,d213,area,xm,ym,zm

	real,parameter :: pi41 =  7.957747154594767E-002
	real,parameter :: f1 =  0.333333333333333
	real,parameter :: f2 =  0.470142064105115
	real,parameter :: f3 =  5.971587178976981E-002
	real,parameter :: f4 =  0.797426985353087
	real,parameter :: f5 =  0.101286507323456
	real,parameter,dimension(7) :: w = (/0.112500000000000,6.619707639425308E-002, &
               6.619707639425308E-002, 6.619707639425308E-002, 6.296959027241358E-002, &
               6.296959027241358E-002, 6.296959027241358E-002/)
end module tri
