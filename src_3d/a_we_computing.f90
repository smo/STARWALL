subroutine a_we_computing
use solv
use tri_p
use mpi_v
use sca

!-----------------------------------------------------------------------
implicit none


if(rank==0) write(*,*) 'a_we_computing starts'

! Set up scalapack sub grid
call SCA_GRID(nd_w,nd_bez)
allocate(a_we_loc(MP_A,NQ_A), stat=ier)
   IF (IER /= 0) THEN
             WRITE (*,*) "Matrrix pe: Can not allocate local matrix A_loc_pwe : MY_PROC_NUM=",MYPNUM, &
                         "MP_A,NQ_A=", MP_A,NQ_A
             STOP
   END IF
   LDA_we=LDA_A
   a_we_loc=0.

!a_wp
CALL DESCINIT(DESCA,nd_w, npot_p, NB, NB, 0, 0, CONTEXT, LDA_wp, INFO_A )
if(INFO_A .NE. 0) then
  write(6,*) "Something is wrong in a_we_computing.f90 CALL DESCINIT DESCA, INFO_A=",INFO_A
  stop
endif

!a_pwe
CALL DESCINIT(DESCB,npot_p, nd_bez, NB, NB, 0, 0, CONTEXT, LDA_pwe_s, INFO_B )
if(INFO_B .NE. 0) then
  write(6,*) "Something is wrong in a_we_computing.f90 CALL DESCINIT DESCB, INFO_B=",INFO_B
  stop
endif

!a_ew
CALL DESCINIT(DESCC,nd_w, nd_bez, NB, NB, 0, 0, CONTEXT, LDA_we, INFO_C )
if(INFO_B .NE. 0) then
  write(6,*) "Something is wrong in a_we_computing  CALL DESCINIT DESCC, INFO_C=",INFO_C
  stop
endif


CALL PDGEMM('N','N', nd_w ,nd_bez, npot_p , 1.0D0  , a_wp_loc ,1,1,DESCA, a_pwe_loc_s,1,1,DESCB,&
             1.0D0, a_we_loc,1 , 1 , DESCC) 

deallocate(a_pwe_loc_s)

if(rank==0) write(*,*) 'a_we_computing complited'
if(rank==0) write(*,*) '==============================================================='

end subroutine a_we_computing

