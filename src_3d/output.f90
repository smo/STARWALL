subroutine output
!-----------------------------------------------------------------------
use mpi_v
implicit none

if(rank==0) write(*,*) 'output begings'

!======================================
call print_starwall_response
!======================================

if(rank==0) write(*,*) 'output ends'
if(rank==0) write(*,*) '==============================================================='

end subroutine output
