subroutine a_ey_computing
use solv
use mpi_v
use sca
use resistive
!-----------------------------------------------------------------------
implicit none


if(rank==0) write(*,*) "a_ey_computing start"

call    SCA_GRID(nd_bez,n_w)
!allocate local matrix
allocate(a_ey_loc(MP_A,NQ_A), stat=ier)
  IF (IER /= 0) THEN
             WRITE (*,*) "a_ey_computing, can not allocate local matrix a_ey_loc MYPROC_NUM=",MYPNUM
             STOP
  END IF
  a_ey_loc=0.
  LDA_ey= LDA_A
  

!a_ey
CALL DESCINIT(DESCA,nd_bez, n_w, NB, NB, 0, 0, CONTEXT, LDA_ey, INFO_A )
if(INFO_A .NE. 0) then
  write(6,*) "Something is wrong in a_ey_computing.f90 CALL DESCINIT DESCA, INFO_A=",INFO_A
  stop
endif

!a_ew
CALL DESCINIT(DESCB,nd_bez, n_w, NB, NB, 0, 0, CONTEXT, LDA_ew, INFO_B )
if(INFO_B .NE. 0) then
  write(6,*) "Something is wrong in a_ey_computing.f90 CALL DESCINIT DESCB, INFO_B=",INFO_B
  stop
endif

!s_ww

CALL DESCINIT(DESCC,n_w, n_w, NB, NB, 0, 0, CONTEXT, LDA_sww, INFO_C )
if(INFO_B .NE. 0) then
  write(6,*) "Something is wrong in a_ey_computing  CALL DESCINIT DESCC, INFO_C=",INFO_C
  stop
endif


CALL PDGEMM('N','N', nd_bez ,n_w, n_w , 1.0D0  , a_ew_loc_sca ,1,1,DESCB, s_ww_loc,1,1,DESCC,&
             1.0D0, a_ey_loc,1 , 1 , DESCA) 


if(rank==0) write(*,*) "a_ey_computing complited"
if(rank==0) write(*,*) '==============================================================='

end subroutine a_ey_computing

