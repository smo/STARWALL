subroutine input

  use icontr
  use contr_su
  use coil2d

  use sca
  use mpi_v
  use resistive

  implicit none
  include "mpif.h"
  integer :: i
  
  ! Namelist with input parameters
  namelist / params / i_response, n_harm, n_tor, nv, delta, n_points, nwall, iwall, nu_coil,&
             NB, ORFAC, lwork_cooficient, format_type
  ! --- Output code information
  if(rank==0)  write(outp,*) '-------------------------------------------'
  if(rank==0)  write(outp,*) 'STARWALL-JOREK vacuum response code'
  if(rank==0)  write(outp,*) '   version 2010-08-10 by P. Merkel'
  if(rank==0)  write(outp,*) 'Parallel version 2016-06-01 by S. Mochalskyy'
  if(rank==0)  write(outp,*) '-------------------------------------------'
  
  ! --- Preset and read input parameters
  i_response = 2
  nv = 32
  delta = 0.001
  n_points = 10
  nwall = 1
  iwall = 1
  nu_coil = 0  
  n_harm = 11
  ncoil=0
  ntri_c=0  

  NB = 64
  ORFAC = 0.00000001
  lwork_cooficient = 3
  format_type = 'unformatted'

  if(n_harm>1) then
     do i=1,n_harm
         n_tor(i)=i
     enddo
  else
         n_tor=0
  endif

  ! Only one task read data from input file and send them after to all tasks
  if(rank==0) read(inp, params) ! Read namelist from STDIN
  !==========================================================================
  call MPI_BCAST(i_response,        1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(n_harm,            1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(n_tor,             n_harm, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(nv,                1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(delta,             1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(n_points,          1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(nwall,             1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(iwall,             1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(nu_coil,           1, MPI_INTEGER, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(NB,                1, MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(ORFAC,             1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(lwork_cooficient,  1, MPI_INTEGER,          0, MPI_COMM_WORLD, ier)
  call MPI_BCAST(format_type,       len(format_type), MPI_CHARACTER,0, MPI_COMM_WORLD, ier)
 !==========================================================================

  ! --- Log input parameters

  if(rank==0) then
       write(outp,'(A,I4)')
       write(outp,*) 'Input parameters:'
       write(outp,'(A,I4)')     '  i_response =', i_response
       write(outp,'(A,I4)')     '  n_harm     =', n_harm
       write(outp,'(A,20I4)')   '  n_tor      =', n_tor(1:n_harm)
       write(outp,'(A,I4)')     '  nv         =', nv
       write(outp,'(A,ES10.3)') '  delta      =', delta
       write(outp,'(A,I4)')     '  n_points   =', n_points
       write(outp,'(A,I4)')     '  nwall      =', nwall
       write(outp,'(A,I4)')     '  iwall      =', iwall
       write(outp,'(A,I4)')     '  nu_coil    =', nu_coil
       write(outp,'(A,I4)')
 
       write(outp,*) ' ScaLAPACK Input parameters:'
       write(outp,'(A,I4)')     '  NB         =', NB
       write(outp,'(A,ES10.3)') '  ORFAC      =', ORFAC
       write(outp,'(A,I4)')

       write(outp,*) 'Output starwall-response.dat:'
       write(outp,'(A,A)')     ' file format      =  ',format_type
       write(outp,'(A,I4)')
  endif


  ! --- Check input parameters
  if ( ( i_response < 0 ) .or. ( i_response > 2 ) ) then
    write(outp,*) 'ERROR: i_response must have a value between 0 and 2'
    stop 1
  end if
  
  if ( n_harm < 1 ) then
    write(outp,*) 'ERROR: n_harm must be one or larger'
    stop 1
  end if

  do i=2,n_harm
    if(n_tor(i).le.n_tor(i-1)) then
      write(outp,*) 'ERROR: toroidal harmonics have to be in increasing order'
      stop 1
    endif
  end do

  if ( ( iwall < 1 ) .or. ( iwall > 3 ) ) then
    write(outp,*) 'ERROR: iwall must have a value between 1 and 3'
    stop 1
  end if 
 
   if ( lwork_cooficient<1) then
    write(outp,*) 'ERROR: lwork_cooficient  must have a value between >1'
    stop 1
  end if
 

  ! --- Output additional information
  if(i_response.le.1 .AND. rank==0)  &
     write(outp,*)'response matrix without wall will be computed' 
  if(i_response.eq.1 .AND. rank==0)  &
     write(outp,*)'response matrix with wall will be computed' 
  if(i_response.eq.2 .AND. rank==0)  then
     write(outp,*)'resistive wall response is computed' 
  end if
  if(rank==0) write(outp,*)
  
  ! --- Derive quantities from input parameters
  nd_harm0 = 0
  nd_harm  = 4*n_harm
  if(n_tor(1).eq.0) then
    nd_harm0 = 2
    nd_harm  = 4*n_harm - 4
  end if
  
end subroutine input
