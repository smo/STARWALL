subroutine d_ee_computing
use solv
use mpi_v
use sca
use resistive
!-----------------------------------------------------------------------
implicit none


if(rank==0) write(*,*) "d_ee_computing start"

call    SCA_GRID(nd_bez,nd_bez)
!allocate local matrix
allocate(d_ee_loc(MP_A,NQ_A), stat=ier)
  IF (IER /= 0) THEN
             WRITE (*,*) "d_ee_computing, can not allocate local matrix d_ee_loc MYPROC_NUM=",MYPNUM
             STOP
  END IF
  d_ee_loc=0.
  LDA_dee= LDA_A

!a_ey
CALL DESCINIT(DESCA,nd_bez,n_w, NB, NB, 0, 0, CONTEXT, LDA_ey, INFO_A )
if(INFO_A .NE. 0) then
  write(6,*) "Something is wrong in d_ee_computing.f90 CALL DESCINIT DESCA, INFO_A=",INFO_A
  stop
endif


!a_ye
CALL DESCINIT(DESCB,n_w,nd_bez, NB, NB, 0, 0, CONTEXT, LDA_ye, INFO_B )
if(INFO_B .NE. 0) then
  write(6,*) "Something is wrong in d_ee_computing.f90 CALL DESCINIT DESCA, INFO_B=",INFO_B
  stop
endif

!d_ee
CALL DESCINIT(DESCC,nd_bez, nd_bez, NB, NB, 0, 0, CONTEXT, LDA_dee, INFO_C )
if(INFO_C .NE. 0) then
  write(6,*) "Something is wrong in d_ee_computing  CALL DESCINIT DESCC, INFO_C=",INFO_C
  stop
endif


CALL PDGEMM('N','N', nd_bez ,nd_bez, n_w , 1.0D0  , a_ey_loc ,1,1,DESCA, a_ye_loc,1,1,DESCB,&
             1.0D0, d_ee_loc,1 , 1 , DESCC) 


if(rank==0) write(*,*) "d_ee_computing complited"
if(rank==0) write(*,*) '==============================================================='

end subroutine d_ee_computing

