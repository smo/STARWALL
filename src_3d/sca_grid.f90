!----------------------------------------------------------------------
subroutine sca_grid(n_matrix_row,n_matrix_col)
! ----------------------------------------------------------------------
!   purpose:                                                  12/01/2016
!   Subroutine creats the scalapack type subgrid 

! ----------------------------------------------------------------------
 
     use sca

! ----------------------------------------------------------------------
     implicit none
    
     integer :: n_matrix_row,n_matrix_col
     integer  NUMROC
     EXTERNAL NUMROC

      ! Blocking factor for ScaLAPACK. Depending on the matrix size -> different performance
      !NB=2     

      MP_A=NUMROC(n_matrix_row, NB, MYROW, 0, NPROW)
      NQ_A=NUMROC(n_matrix_col, NB, MYCOL, 0, NPCOL)

      LDA_A= MAX(1, MP_A);
 
end subroutine sca_grid
