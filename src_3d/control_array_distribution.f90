   subroutine control_array_distribution
! ----------------------------------------------------------------------
   use mpi_v 
   use tri_w
   use tri_p
   use coil2d
   use sca
   implicit none
   include "mpif.h"
    
   step=(npot_w+ncoil)/NPROCS
   if(step<1) then
          if(rank==0) then
              write(6,*) "Number of MPI task more than value of npot_w+ncoil; ",&
                     "npot_w+ncoil=",npot_w+ncoil, "n_task=",NPROCS
              call MPI_BARRIER(MPI_COMM_WORLD)
              stop
          endif
   endif
       

   step=(npot_p)/NPROCS
   if(step<1) then
          if(rank==0) then
                write(6,*) "Number of MPI task more than value of npot_p; ",&
                           "npot_p=",npot_p, "n_task=",NPROCS
                call MPI_BARRIER(MPI_COMM_WORLD)
                stop
          endif
   endif


end subroutine control_array_distribution

