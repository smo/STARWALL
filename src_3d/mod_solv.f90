module solv

   implicit none
   real,dimension(:,:),allocatable ::  a_pp_loc, a_wp_loc, a_ww_loc, a_rw_loc, a_pwe_loc,a_pwe_loc_s
   real,dimension(:,:),allocatable ::  a_ew_loc_sca,a_ep_loc_sca,a_we_loc
   real,dimension(:,:),allocatable ::  a_ee_loc
 
   integer :: nd_we,nd_w,nd_bez,nd_e
   integer :: npot_p_loc_b, npot_p_loc_e, npot_w_loc_b, npot_w_loc_e

end module solv
