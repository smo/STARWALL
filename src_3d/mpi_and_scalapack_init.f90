   subroutine MPI_and_Scalapack_Init
! ----------------------------------------------------------------------
   
   use mpi_v
   use time
   implicit none
   include "mpif.h"
    

   call MPI_INIT(ier)
   if (ier .ne. MPI_SUCCESS) then
          print *,'Error starting MPI program. Terminating!!'
          call MPI_ABORT(MPI_COMM_WORLD, ERRORCODE, ier)
   endif
   call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ier)
   call MPI_COMM_SIZE(MPI_COMM_WORLD, numtasks, ier)

   !In order to measure total wallclock time
   call MPI_BARRIER(MPI_COMM_WORLD,ier)
   time1=MPI_WTIME()

   !Distribute MPI task on the ScaLapack process grid
   call MPI_GRID()
       
end subroutine MPI_and_Scalapack_Init

