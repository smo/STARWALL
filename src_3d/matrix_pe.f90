!----------------------------------------------------------------------
subroutine matrix_pe
! ----------------------------------------------------------------------
!   purpose:                                                  10/08/09
!
! ----------------------------------------------------------------------
use icontr
use contr_su

use solv
use tri_p
use tri_w
use coil2d

use mpi_v
use sca
! ----------------------------------------------------------------------
implicit none

real,dimension(0:n_points) :: h11_s,h12_s,h21_s,h22_s,eps
real                       :: pi2,fnvp,fnb,alv,s,co,si  
integer                    :: i,i0,ip,j,j1,jump,k,k1,k2,kv
integer                    :: nuv,k_start
integer                    :: i_node1,i_node2
integer                    :: k11, k12, k21, k22, k1_dof, k2_dof

integer :: j_loc,i_loc
logical :: inside_j,inside_i

! ----------------------------------------------------------------------
if(rank==0) write(*,*) 'compute  matrix_pe'
! ----------------------------------------------------------------------


pi2  = 4.*asin(1.)
jump = 0
nuv  = nu*nv
fnb  = 1./float(N_bnd)
fnvp = 1./float(nv*n_points)
alv  = pi2/float(nv)

k_start = 1
eps           =  1.
eps(       0) = .5
eps(n_points) = .5

do i=0,n_points

  s = float(i)/float(n_points)
  call basisfunctions1(s,H,H_s,H_ss)

  h11_s(i) =  H_s(1,1)*fnvp*eps(i)
  h12_s(i) =  H_s(1,2)*fnvp*eps(i)
  h21_s(i) =  H_s(2,1)*fnvp*eps(i)
  h22_s(i) =  H_s(2,2)*fnvp*eps(i)

enddo

nd_we = ncoil + npot_w +(nd_harm+nd_harm0)/2*N_dof_bnd

if(rank==0) write(*,*) '  MATRIX_PE : npot_p : ',npot_p
if(rank==0) write(*,*) '  MATRIX_PE : nd_we  : ',nd_we
if(rank==0) write(*,*) '  MATRIX_PE : dim    : ',npot_p, nd_we

 ! Set up scalapack sub grid
   call SCA_GRID(npot_p,nd_we)
   ! allocate local matrix
   allocate(a_pwe_loc(MP_A,NQ_A), stat=ier)
   IF (IER /= 0) THEN
             WRITE (*,*) "Matrrix pe: Can not allocate local matrix A_loc_pwe : MY_PROC_NUM=",MYPNUM, &
                         "MP_A,NQ_A=", MP_A,NQ_A
             STOP
   END IF
   LDA_pwe=LDA_A
   a_pwe_loc=0.

! contribution  harmonic n=0
if (n_tor(1).eq.0) then !!!!  TO BE ADAPTED FOR CORNERS  !!!!
  k_start = 3
  call ScaLAPACK_mapping_i(1,i_loc,inside_i)
     if (inside_i) then
         do k = 1,N_bnd           ! boundary elements
            k1 = (nd_harm+nd_harm0)*(k-1) + 1 + npot_w + ncoil
            call ScaLAPACK_mapping_j(k1,j_loc,inside_j)
                 if (inside_j) then
                    a_pwe_loc(i_loc,j_loc) =  fnb
                 endif
          enddo
     endif

  do kv=1,nv
    do k = 1,N_bnd           ! boundary elements TO BE ADAPTED FOR CORNERS
      k1 = (nd_harm +    nd_harm0)*(k-1)                 + 1 + npot_w + ncoil
      k2 = mod((nd_harm+nd_harm0)* k   ,(nd_harm+nd_harm0)*N_bnd) + 1 + npot_w + ncoil

      do i=0,n_points

        j  = mod(i + n_points*(k-1),nu) + 1 + (kv-1)*nu

        if (j.ne.nuv) then
             call ScaLAPACK_mapping_i(j+1,i_loc,inside_i)
             if (inside_i) then
                 call ScaLAPACK_mapping_j(k1,j_loc,inside_j)
                 if (inside_j) then
                       a_pwe_loc(i_loc,j_loc  ) = a_pwe_loc(i_loc,j_loc) - h11_s(i) * Lij(1,1,k)
                 endif
 
                 call ScaLAPACK_mapping_j(k2,j_loc,inside_j)
                 if (inside_j) then
                       a_pwe_loc(i_loc,j_loc  ) = a_pwe_loc(i_loc,j_loc)  - h21_s(i) * Lij(2,1,k)
                 endif

                 call ScaLAPACK_mapping_j(k1+1,j_loc,inside_j)
                 if (inside_j) then
                       a_pwe_loc(i_loc,j_loc  ) = a_pwe_loc(i_loc,j_loc)  - h12_s(i) * Lij(1,2,k)
                 endif

                 call ScaLAPACK_mapping_j(k2+1,j_loc,inside_j)
                 if (inside_j) then
                       a_pwe_loc(i_loc,j_loc  ) = a_pwe_loc(i_loc,j_loc)  - h22_s(i) * Lij(2,2,k)
                 endif

             endif
        endif

      enddo
    enddo
  enddo

endif



! end contribution  harmonic n=0
!----------------------------------------------------------------------


if ( (n_tor(1) .ne. 0) .or. (n_harm .gt. 1) ) then

  i0 = 1

  if ( n_tor(1) .eq. 0 ) i0 = 2

  do i=i0,n_harm      ! toroidal harmonics

    jump = 4*(i-i0) + npot_w + ncoil

    do kv=1,nv

      co = cos(alv*n_tor(i)*(kv-1))
      si = sin(alv*n_tor(i)*(kv-1))

      do k = 1,N_bnd           ! boundary elements    

        i_node1 = bnd_node(k,1)     ! the first node of this element (in the list with double entries!)
        i_node2 = bnd_node(k,2)     ! the second node of this element (in the list with double entries!)

        k11 = k_start + (nd_harm+nd_harm0)/2 *(bnd_node_index(i_node1,1)-1) + jump
        k12 = k11     +  bnd_node_index(i_node1,2) - bnd_node_index(i_node1,1)

        k21 = k_start + (nd_harm+nd_harm0)/2 *(bnd_node_index(i_node2,1)-1) + jump
        k22 = k21     +  bnd_node_index(i_node2,2) - bnd_node_index(i_node2,1)

        k1_dof = bnd_node_ndof(i_node1)
        k2_dof = bnd_node_ndof(i_node2)

        do ip = 0, n_points

          j  = mod(ip + n_points*(k-1),nu) + 1 + (kv-1)*nu
          j1 = j+1

         ! if (kv .eq. 1) write(*,*) ip,j1

            if (j.ne.nuv) then
 

             call ScaLAPACK_mapping_i(j1,i_loc,inside_i)
             if (inside_i) then
                 call ScaLAPACK_mapping_j(k11,j_loc,inside_j)
                 if (inside_j) then
                       a_pwe_loc(i_loc,j_loc  ) = a_pwe_loc(i_loc,j_loc)   + co * h11_s(ip) * Lij(1,1,k)
                 endif

                 call ScaLAPACK_mapping_j(k12,j_loc,inside_j)
                 if (inside_j) then
                       a_pwe_loc(i_loc,j_loc  ) = a_pwe_loc(i_loc,j_loc)  + co * h12_s(ip) * Lij(1,2,k)
                 endif

                 call ScaLAPACK_mapping_j(k11+k1_dof,j_loc,inside_j)
                 if (inside_j) then
                       a_pwe_loc(i_loc,j_loc  ) = a_pwe_loc(i_loc,j_loc)  + si * h11_s(ip) * Lij(1,1,k)
                 endif

                 call ScaLAPACK_mapping_j(k12+k1_dof,j_loc,inside_j)
                 if (inside_j) then
                       a_pwe_loc(i_loc,j_loc  ) = a_pwe_loc(i_loc,j_loc)  + si * h12_s(ip) * Lij(1,2,k)
                 endif

                 call ScaLAPACK_mapping_j(k21,j_loc,inside_j)
                 if (inside_j) then
                       a_pwe_loc(i_loc,j_loc  ) = a_pwe_loc(i_loc,j_loc)  + co * h21_s(ip) * Lij(2,1,k)
                 endif

                 call ScaLAPACK_mapping_j(k22,j_loc,inside_j)
                 if (inside_j) then
                       a_pwe_loc(i_loc,j_loc  ) = a_pwe_loc(i_loc,j_loc)  + co * h22_s(ip) * Lij(2,2,k)
                 endif

                 call ScaLAPACK_mapping_j(k21+k2_dof,j_loc,inside_j)
                 if (inside_j) then
                       a_pwe_loc(i_loc,j_loc  ) = a_pwe_loc(i_loc,j_loc)  + si * h21_s(ip) * Lij(2,1,k)
                 endif

                 call ScaLAPACK_mapping_j(k22+k2_dof,j_loc,inside_j)
                 if (inside_j) then
                       a_pwe_loc(i_loc,j_loc  ) = a_pwe_loc(i_loc,j_loc)  + si * h22_s(ip) * Lij(2,2,k)
                 endif
              endif 
           endif
        enddo
      enddo
    enddo
  enddo

endif



!----------------------------------------------------------------------
if(rank==0) write(*,*) 'matrix_pe done' 
if(rank==0) write(*,*) '==============================================================='
end subroutine matrix_pe

