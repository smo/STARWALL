subroutine print_starwall_response

use contr_su
use solv
use tri_w
use coil2d

use sca
use mpi_v
use resistive
!-----------------------------------------------------------------------
implicit none
include "mpif.h"


integer  :: i,j,i_loc,j_loc
real,dimension(:,:),allocatable ::  a_ye_print(:,:),a_ey_print(:,:),a_ee_print(:,:), &
                                    s_ww_print(:,:),s_ww_inv_print(:,:)
integer  :: IC, JC
integer  INDXL2G
EXTERNAL INDXL2G

!-----------------------------------------------------------------------

! (Determine positions of wall triangle nodes)
if(rank==0) then
   allocate( xyzpot_w(npot_w,3) )
   do i = 1, ntri_w
      do j = 1, 3
         xyzpot_w(jpot_w(i,j),:) = (/ xw(i,j), yw(i,j), zw(i,j) /)
      end do
   end do
endif

if (format_type == 'formatted' ) then
  if(rank==0) then
       open(60, file='starwall-response.dat', form=format_type, status='replace', action='write')
       130 format(a)
       131 format('#@intparam  ',a24,99i12)
       132 format('#@array     ',a24,i12,a24,99i12)
       133 format(4es24.16)
       134 format(8i12)

       write(60,130) '#@content STARWALL VACUUM RESPONSE DATA FOR THE JOREK CODE'
       write(60,131) 'file_version', 1
       write(60,131) 'n_bnd', N_bnd
       write(60,131) 'nd_bez', nd_bez
       write(60,131) 'ncoil', ncoil
       write(60,131) 'npot_w', npot_w
       write(60,131) 'n_w', n_w
       write(60,131) 'ntri_w', ntri_w
       write(60,131) 'n_tor', n_tor_jorek
       write(60,132) 'i_tor', 1, 'int', n_tor_jorek, 0
       write(60,134) i_tor_jorek(1:n_tor_jorek)
       write(60,132) 'yy', 1, 'float', n_w, 0
       write(60,133) 1./gamma(:)
       write(60,132) 'ye', 2, 'float', n_w, nd_bez
       
   endif
      
   allocate(a_ye_print(n_w,nd_bez), stat=ier)
           IF (IER /= 0) THEN
               WRITE (*,*) "output.f90, can not allocate a_ye_print  MYPROC_NUM=",MYPNUM
               STOP
           END IF
   a_ye_print=0.

   call    SCA_GRID(n_w,nd_bez)
   DO i_loc = 1,MP_A
        IC= INDXL2G( i_loc,  NB,  MYROW,   0, NPROW)
        DO j_loc = 1,NQ_A
              JC= INDXL2G(   j_loc,  NB,  MYCOL,  0, NPCOL)
              a_ye_print(IC,JC) = a_ye_loc(i_loc,j_loc)
        END DO
   END DO
  

   if(rank==0) then
        call MPI_REDUCE(MPI_IN_PLACE, a_ye_print, n_w*nd_bez, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)     
   else
        call MPI_REDUCE(a_ye_print, a_ye_print, n_w*nd_bez, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   endif


   if(rank==0)    write(60,133) a_ye_print(:,:)
   deallocate (a_ye_print, a_ye_loc) 

!=========================================================================================

   allocate(a_ey_print(nd_bez,n_w),stat=ier)
           IF (IER /= 0) THEN
               WRITE (*,*) "output.f90, can not allocate a_ey_print  MYPROC_NUM=",MYPNUM
               STOP
           END IF
   a_ey_print=0.

   call    SCA_GRID(nd_bez,n_w)
   DO i_loc = 1,MP_A
        IC= INDXL2G( i_loc,  NB,  MYROW,   0, NPROW)
        DO j_loc = 1,NQ_A
              JC= INDXL2G(   j_loc,  NB,  MYCOL,  0, NPCOL)
              a_ey_print(IC,JC) = a_ey_loc(i_loc,j_loc)
        END DO
   END DO

   if(rank==0) then
        call MPI_REDUCE(MPI_IN_PLACE, a_ey_print, nd_bez*n_w, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   else
         call MPI_REDUCE(a_ey_print, a_ey_print, nd_bez*n_w, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)

   endif

  if(rank==0) then
     write(60,132) 'ey', 2, 'float', nd_bez, n_w
     write(60,133) a_ey_print(:,:)
  endif
  deallocate (a_ey_print, a_ey_loc)

!=========================================================================================

  allocate(a_ee_print(nd_bez,nd_bez),stat=ier)
           IF (IER /= 0) THEN
               WRITE (*,*) "output.f90, can not allocate a_ee_print  MYPROC_NUM=",MYPNUM
               STOP
           END IF
  a_ee_print=0.
  call    SCA_GRID(nd_bez,nd_bez)
  DO i_loc = 1,MP_A
        IC= INDXL2G( i_loc,  NB,  MYROW,   0, NPROW)
        DO j_loc = 1,NQ_A
              JC= INDXL2G(   j_loc,  NB,  MYCOL,  0, NPCOL)
              a_ee_print(IC,JC) = a_ee_loc(i_loc,j_loc)
        END DO
   END DO

   if(rank==0) then
       call MPI_REDUCE(MPI_IN_PLACE, a_ee_print, nd_bez*nd_bez, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   else
       call MPI_REDUCE(a_ee_print, a_ee_print, nd_bez*nd_bez, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   endif

   if(rank==0) then
     write(60,132) 'ee', 2, 'float', nd_bez, nd_bez
     write(60,133) a_ee_print(:,:)
   endif
  deallocate (a_ee_print, a_ee_loc)

!=========================================================================================

  allocate(s_ww_print(n_w,n_w),stat=ier)
           IF (IER /= 0) THEN
               WRITE (*,*) "output.f90, can not allocate s_ww_print  MYPROC_NUM=",MYPNUM
               STOP
           END IF
 s_ww_print=0.

  call  SCA_GRID(n_w,n_w)
  DO i_loc = 1,MP_A
        IC= INDXL2G( i_loc,  NB,  MYROW,   0, NPROW)
        DO j_loc = 1,NQ_A
              JC= INDXL2G(   j_loc,  NB,  MYCOL,  0, NPCOL)
              s_ww_print(IC,JC) = s_ww_loc(i_loc,j_loc)
        END DO
  END DO

  if(rank==0) then
       call MPI_REDUCE(MPI_IN_PLACE, s_ww_print, n_w*n_w, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   else
       call MPI_REDUCE(s_ww_print, s_ww_print, n_w*n_w, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   endif

   if(rank==0) then
     write(60,132) 's_ww', 2, 'float', n_w, n_w
     write(60,133) s_ww_print(:,:)
   endif
   deallocate (s_ww_print, s_ww_loc)
!=========================================================================================
   allocate(s_ww_inv_print(n_w,n_w),stat=ier)
           IF (IER /= 0) THEN
               WRITE (*,*) "output.f90, can not allocate s_ww_inv_print  MYPROC_NUM=",MYPNUM
               STOP
           END IF
  s_ww_inv_print=0.
  call  SCA_GRID(n_w,n_w)
  DO i_loc = 1,MP_A
        IC= INDXL2G( i_loc,  NB,  MYROW,   0, NPROW)
        DO j_loc = 1,NQ_A
              JC= INDXL2G(   j_loc,  NB,  MYCOL,  0, NPCOL)
              s_ww_inv_print(IC,JC) = s_ww_inv_loc(i_loc,j_loc)
        END DO
  END DO

   if(rank==0) then
       call MPI_REDUCE(MPI_IN_PLACE, s_ww_inv_print, n_w*n_w, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   else
       call MPI_REDUCE(s_ww_inv_print, s_ww_inv_print, n_w*n_w, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   endif

   if(rank==0) then
     write(60,132) 's_ww_inv', 2, 'float', n_w, n_w
     write(60,133) s_ww_inv_print(:,:)
  endif
  deallocate (s_ww_inv_print, s_ww_inv_loc)


!=========================================================================================
  if(rank==0) then
     write(60,132) 'xyzpot_w', 2, 'float', npot_w, 3
     write(60,133) xyzpot_w(:,:)
     write(60,132) 'jpot_w', 2, 'int', ntri_w, 3
     write(60,134) jpot_w(:,:)

     deallocate(xyzpot_w)

  endif
  deallocate(jpot_w)
!=========================================================================================
else  !if (format_type == 'unformatted' ) then

   if(rank==0) then

      open(60, file='starwall-response.dat', form=format_type, status='replace', action='write')

      char512='#@content STARWALL VACUUM RESPONSE DATA FOR THE JOREK CODE'
      write(60) 42, 42.d0 !### for an elementary check in JOREK
      write(60) char512
      write(60) '#@intparam  ', 'file_version            ', 1
      write(60) '#@intparam  ', 'n_bnd                   ', N_bnd
      write(60) '#@intparam  ', 'nd_bez                  ', nd_bez
      write(60) '#@intparam  ', 'ncoil                   ', ncoil
      write(60) '#@intparam  ', 'npot_w                  ', npot_w
      write(60) '#@intparam  ', 'n_w                     ', n_w
      write(60) '#@intparam  ', 'ntri_w                  ', ntri_w
      write(60) '#@intparam  ', 'n_tor                   ', n_tor_jorek
      write(60) '#@array     ', 'i_tor                   ', 1, 'int                     ', n_tor_jorek, 0
      write(60) i_tor_jorek(1:n_tor_jorek)
      write(60) '#@array     ', 'yy                      ', 1, 'float                   ', n_w, 0
      write(60) 1.d0/gamma(:)
      write(60) '#@array     ', 'ye                      ', 2, 'float                   ', n_w, nd_bez

   endif

   allocate(a_ye_print(n_w,nd_bez), stat=ier)
           IF (IER /= 0) THEN
               WRITE (*,*) "output.f90, can not allocate a_ye_print  MYPROC_NUM=",MYPNUM
               STOP
           END IF
   a_ye_print=0.

   call    SCA_GRID(n_w,nd_bez)
   DO i_loc = 1,MP_A
        IC= INDXL2G( i_loc,  NB,  MYROW,   0, NPROW)
        DO j_loc = 1,NQ_A
              JC= INDXL2G(   j_loc,  NB,  MYCOL,  0, NPCOL)
              a_ye_print(IC,JC) = a_ye_loc(i_loc,j_loc)
        END DO
   END DO

   if(rank==0) then
        call MPI_REDUCE(MPI_IN_PLACE, a_ye_print, n_w*nd_bez, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   else
        call MPI_REDUCE(a_ye_print, a_ye_print, n_w*nd_bez, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   endif


   if(rank==0)    write(60) a_ye_print(:,:)
   deallocate (a_ye_print, a_ye_loc)

!=========================================================================================

   allocate(a_ey_print(nd_bez,n_w),stat=ier)
           IF (IER /= 0) THEN
               WRITE (*,*) "output.f90, can not allocate a_ey_print  MYPROC_NUM=",MYPNUM
               STOP
           END IF
   a_ey_print=0.

   call    SCA_GRID(nd_bez,n_w)
   DO i_loc = 1,MP_A
        IC= INDXL2G( i_loc,  NB,  MYROW,   0, NPROW)
        DO j_loc = 1,NQ_A
              JC= INDXL2G(   j_loc,  NB,  MYCOL,  0, NPCOL)
              a_ey_print(IC,JC) = a_ey_loc(i_loc,j_loc)
        END DO
   END DO

   if(rank==0) then
        call MPI_REDUCE(MPI_IN_PLACE, a_ey_print, nd_bez*n_w, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   else
         call MPI_REDUCE(a_ey_print, a_ey_print, nd_bez*n_w, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)

   endif

  if(rank==0) then
     write(60) '#@array     ', 'ey                      ', 2, 'float                   ', nd_bez, n_w
     write(60) a_ey_print(:,:)
  endif
  deallocate (a_ey_print, a_ey_loc)

!=========================================================================================

  allocate(a_ee_print(nd_bez,nd_bez),stat=ier)
           IF (IER /= 0) THEN
               WRITE (*,*) "output.f90, can not allocate a_ee_print  MYPROC_NUM=",MYPNUM
               STOP
           END IF
  a_ee_print=0.
  call    SCA_GRID(nd_bez,nd_bez)
  DO i_loc = 1,MP_A
        IC= INDXL2G( i_loc,  NB,  MYROW,   0, NPROW)
        DO j_loc = 1,NQ_A
              JC= INDXL2G(   j_loc,  NB,  MYCOL,  0, NPCOL)
              a_ee_print(IC,JC) = a_ee_loc(i_loc,j_loc)
        END DO
   END DO

   if(rank==0) then
       call MPI_REDUCE(MPI_IN_PLACE, a_ee_print, nd_bez*nd_bez, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   else
       call MPI_REDUCE(a_ee_print, a_ee_print, nd_bez*nd_bez, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   endif

   if(rank==0) then
     write(60) '#@array     ', 'ee                      ', 2, 'float                   ', nd_bez, nd_bez
     write(60) a_ee_print(:,:)
   endif
  deallocate (a_ee_print, a_ee_loc)
!=========================================================================================

  allocate(s_ww_print(n_w,n_w),stat=ier)
           IF (IER /= 0) THEN
               WRITE (*,*) "output.f90, can not allocate s_ww_print  MYPROC_NUM=",MYPNUM
               STOP
           END IF
 s_ww_print=0.

  call  SCA_GRID(n_w,n_w)
  DO i_loc = 1,MP_A
        IC= INDXL2G( i_loc,  NB,  MYROW,   0, NPROW)
        DO j_loc = 1,NQ_A
              JC= INDXL2G(   j_loc,  NB,  MYCOL,  0, NPCOL)
              s_ww_print(IC,JC) = s_ww_loc(i_loc,j_loc)
        END DO
  END DO

  if(rank==0) then
       call MPI_REDUCE(MPI_IN_PLACE, s_ww_print, n_w*n_w, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   else
       call MPI_REDUCE(s_ww_print, s_ww_print, n_w*n_w, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   endif

   if(rank==0) then
     write(60) '#@array     ', 's_ww                    ', 2, 'float                   ', n_w, n_w
     write(60) s_ww_print(:,:)
   endif
   deallocate (s_ww_print, s_ww_loc)
!=========================================================================================
   allocate(s_ww_inv_print(n_w,n_w),stat=ier)
           IF (IER /= 0) THEN
               WRITE (*,*) "output.f90, can not allocate s_ww_inv_print  MYPROC_NUM=",MYPNUM
               STOP
           END IF
  s_ww_inv_print=0.
  call  SCA_GRID(n_w,n_w)
  DO i_loc = 1,MP_A
        IC= INDXL2G( i_loc,  NB,  MYROW,   0, NPROW)
        DO j_loc = 1,NQ_A
              JC= INDXL2G(   j_loc,  NB,  MYCOL,  0, NPCOL)
              s_ww_inv_print(IC,JC) = s_ww_inv_loc(i_loc,j_loc)
        END DO
  END DO

   if(rank==0) then
       call MPI_REDUCE(MPI_IN_PLACE, s_ww_inv_print, n_w*n_w, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   else
       call MPI_REDUCE(s_ww_inv_print, s_ww_inv_print, n_w*n_w, &
                   MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, IER)
   endif

   if(rank==0) then
     write(60) '#@array     ', 's_ww_inv                ', 2, 'float                   ', n_w, n_w
     write(60) s_ww_inv_print(:,:)
   endif
  deallocate (s_ww_inv_print, s_ww_inv_loc)


!=========================================================================================
  if(rank==0) then
        write(60) '#@array     ', 'xyzpot_w                ', 2, 'float                   ', npot_w, 3
        write(60) xyzpot_w(:,:)
        write(60) '#@array     ', 'jpot_w                  ', 2, 'int                     ', ntri_w, 3
        write(60) jpot_w(:,:)

        deallocate (xyzpot_w)
  endif
  deallocate (jpot_w)
!=========================================================================================


end if !if (format_type == 'formatted' ) then

close(60)

end subroutine print_starwall_response
