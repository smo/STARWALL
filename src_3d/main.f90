program  starwall
! ----------------------------------------------------------------------
   use icontr
   use mpi_v

   implicit none
   include "mpif.h"
    
   ! Initizlization of MPI and Scalapack process grid
   call MPI_and_Scalapack_Init

   ! --- Read input parameters from namelist file.
   call input

   ! --- Read the JOREK control boundary.
   call control_boundary
   
  ! --- Generation of the control surface triangles
   call tri_contr_surf
  
   ! --- Discretization of the wall (generate wall triangles manually)
   if ((nwall .gt. 0) .and. (iwall .eq. 1)) then 
     call surface_wall
   end if


   ! --- Read wall triangles
   !if(nwall.gt.0.and.iwall.eq.2)  then 
   !call read_wall_triangles
   !end if

   ! --- Read external coils (not tested yet)
   if (nu_coil .gt. 0) call read_coil_data
  
   ! --- Solve Laplace equation Delta Phi = 0 as a variational problem to determine
   !    the unkowns (current potentials Phi on wall triangle vertices)
   !    Boundary conditions: B_perp on control surface from JOREK
   !                         B_perp on wall is zero (ideal conductor)

   ! Check if the amount of MPI tasks more than array dimencions
    call control_array_distribution

   ! Main solver
   call solver

   ! --- Write out the response matrices
   if (i_response .le. 1) then
       !call ideal_wall_response
   endif
   if (i_response .eq. 2) then
     call resistive_wall_response
   endif

   call MPI_FINALIZE(ier)

end program starwall


