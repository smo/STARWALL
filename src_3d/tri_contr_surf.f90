! ----------------------------------------------------------------------
      subroutine tri_contr_surf
! ----------------------------------------------------------------------!
!     purpose:
! ----------------------------------------------------------------------
     use icontr
     use tri_p
     use contr_su

     use mpi_v
! ----------------------------------------------------------------------
    implicit none
    
    real,dimension(:),allocatable :: xs,ys,zs
    real                          :: pi2,alv,fnu,co,si
    integer                       :: i,ip,j,ku,kv,i1,i2,i3,i4,nuv
! ----------------------------------------------------------------------

    nuv  = nu*nv
    ntri_p = 2*nuv

    allocate(xp(2*nuv,3),yp(2*nuv,3),zp(2*nuv,3),jpot_p(2*nuv,3)       &
            ,phi0_p(ntri_p,3),xs(nuv),ys(nuv),zs(nuv),stat=ier        )
    IF (IER /= 0) THEN
             WRITE (*,*) "tri_contr_surf.f90  Can not allocate local arrays: MY_PROC_NUM=",rank
             STOP
    END IF

! ----------------------------------------------------------------------
    pi2 = 4.*asin(1.)
    alv = pi2/float(nv)
    fnu = 1./float(nu)
    npot_p = nuv 
    npot_p1 = npot_p-1
    j=0
    do  kv = 1,nv
           co = cos(alv*(kv-1))
           si = sin(alv*(kv-1))
        do  ku = 1,nu
            j=j+1 
            xs(j) = Rc(ku)*co
            ys(j) = Rc(ku)*si
            zs(j) = Zc(ku)
      enddo
    enddo
! ----------------------------------------------------------------------
      j=0 
      do  kv = 1,nv
         ip   = nu 
      if(kv.eq.nv)   &
         ip   = -nuv +nu
       do ku=1,nu
         i3   = ku+nu*(kv-1)
         i4   = mod(ku,nu)+1+nu*(kv-1)
         i1   = i3+ip
         i2   = i4+ip
         i    = 2*i3-1
         jpot_p(i,  1) = i1
         jpot_p(i,  2) = i2
         jpot_p(i,  3) = i3
         jpot_p(i+1,1) = i4
         jpot_p(i+1,2) = i3
         jpot_p(i+1,3) = i2

         phi0_p(i  ,1) = (float(ku)-1.)*fnu
         phi0_p(i  ,2) = (float(ku)   )*fnu
         phi0_p(i  ,3) = (float(ku)-1.)*fnu
         phi0_p(i+1,1) = (float(ku)   )*fnu
         phi0_p(i+1,2) = (float(ku)-1.)*fnu
         phi0_p(i+1,3) = (float(ku)   )*fnu


      enddo
      enddo
      do   i=1,ntri_p
        xp(i,:) = xs(jpot_p(i,:))
        yp(i,:) = ys(jpot_p(i,:))
        zp(i,:) = zs(jpot_p(i,:))
      enddo
      if(rank==0) write(6,*) 
      if(rank==0) write(6,*) 'number of triangles  ntri_p= ',ntri_p
      if(rank==0) write(6,*) 'dimension of current potential array npot_p = ',npot_p
     deallocate(zs,ys,xs)
     end subroutine tri_contr_surf
