module tri_b

	implicit none
	
	real, dimension(:),allocatable :: nx_b,ny_b,nz_b,&
	tx1_b,ty1_b,tz1_b,tx2_b,ty2_b,tz2_b,tx3_b,ty3_b,tz3_b,	   &
	d221_b,d232_b,d213_b,area_b,xm_b,ym_b,zm_b

	real,parameter :: pi41 =  7.957747154594767E-002
	real,parameter :: f1 =  0.333333333333333
	real,parameter :: f2 =  0.470142064105115
	real,parameter :: f3 =  5.971587178976981E-002
	real,parameter :: f4 =  0.797426985353087
	real,parameter :: f5 =  0.101286507323456
	real,parameter,dimension(7) :: w = (/0.112500000000000,6.619707639425308E-002, &
							6.619707639425308E-002, 6.619707639425308E-002, 6.296959027241358E-002, &
							6.296959027241358E-002, 6.296959027241358E-002/)					
end module tri_b
