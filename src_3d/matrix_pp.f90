!----------------------------------------------------------------------
subroutine matrix_pp
! ----------------------------------------------------------------------
!   purpose:                                                  16/11/05
!
! ----------------------------------------------------------------------
use solv
use tri_p
use contr_su
use tri ! new module with arrays and variables for tri_induct subroutine
use mpi_v ! new module with mpi variables
use sca ! new module for ScaLAPACK variables

! ----------------------------------------------------------------------
implicit none
include 'mpif.h'


real,   dimension(:,:),allocatable :: dxp,dyp,dzp
real,   dimension(  :),allocatable :: jx,jy,jz,jdx,jdy,jdz,jdx2,jdy2,jdz2
integer,dimension(:,:),allocatable :: ipot_p
real                               :: temp,aa
integer                            :: i,i1,j,j1,k,k1,ku,ku2,nu2
real                               :: dima_sca,dima_sca2,dima_sca3

integer    j_loc,i_loc
logical    inside_j,inside_i



! ----------------------------------------------------------------------
if (rank==0) write(6,*) '==============================================================='
if (rank==0) write(6,*) 'compute  matrix_pp npot_p=',npot_p
! ----------------------------------------------------------------------


allocate(dxp(ntri_p,3), dyp(ntri_p,3), dzp(ntri_p,3),               &
         ipot_p(ntri_p,3),jx(ntri_p),  jy(ntri_p), jz(ntri_p),      &
         jdx(ntri_p), jdy(ntri_p), jdz(ntri_p),                     &
         jdx2(ntri_p), jdy2(ntri_p), jdz2(ntri_p),  stat=ier)
         if (ier /= 0) then
             print *,'matrix_pp.f90: Can not allocate local arrays dxp, ..., z1'
             stop
         endif


dxp=0.; dyp=0.; dzp=0.; ipot_p=0
jx=0.; jy=0.; jz=0.; jdx=0.; jdy=0.; jdz=0.; jdx2=0.; jdy2=0.; jdz2=0.

! Additional arrays that will be used for tri_induct subroutine

 allocate(nx(ntri_p),ny(ntri_p),nz(ntri_p),tx1(ntri_p),         &
          ty1(ntri_p),tz1(ntri_p),tx2(ntri_p),ty2(ntri_p),      &
          tz2(ntri_p),tx3(ntri_p),ty3(ntri_p),tz3(ntri_p),      &
          d221(ntri_p),d232(ntri_p),d213(ntri_p),area(ntri_p),  &
          xm(7*ntri_p), ym(7*ntri_p), zm(7*ntri_p),stat=ier)
          if (ier /= 0) then
             print *,'matrix_pp.f90: Can not allocate local arrays nx, ..., zm'
             stop
          endif

nx=0.; ny=0.; nz=0.; tx1=0.; ty1=0.;tz1=0.; tx2=0.
ty2=0.; tz2=0.; tx3=0.; ty3=0.; tz3=0.; d221=0.
d232=0.; d213=0.;area=0.; xm=0.; ym=0.; zm=0.
! ----------------------------------------------------------------------

ipot_p = jpot_p
nu2 = 2*nu

do i = 1,ntri_p
  dxp(i,1) = xp(i,2) - xp(i,3)
  dyp(i,1) = yp(i,2) - yp(i,3)
  dzp(i,1) = zp(i,2) - zp(i,3)
  dxp(i,2) = xp(i,3) - xp(i,1)
  dyp(i,2) = yp(i,3) - yp(i,1)
  dzp(i,2) = zp(i,3) - zp(i,1)
  dxp(i,3) = xp(i,1) - xp(i,2)
  dyp(i,3) = yp(i,1) - yp(i,2)
  dzp(i,3) = zp(i,1) - zp(i,2)

  call tri_induct_1(i,xp,yp,zp,ntri_p)
  call tri_induct_2(ntri_p,i,xp,yp,zp)

  jx(i) = phi0_p(i,1)*dxp(i,1)+phi0_p(i,2)*dxp(i,2)+phi0_p(i,3)*dxp(i,3)
  jy(i) = phi0_p(i,1)*dyp(i,1)+phi0_p(i,2)*dyp(i,2)+phi0_p(i,3)*dyp(i,3)
  jz(i) = phi0_p(i,1)*dzp(i,1)+phi0_p(i,2)*dzp(i,2)+phi0_p(i,3)*dzp(i,3)

enddo


!Set up scalapack sub grid
   call SCA_GRID(npot_p,npot_p)
!allocate local matrix
    allocate(A_pp_loc(MP_A,NQ_A), stat=ier)
    IF (IER /= 0) THEN
             WRITE (*,*) "matrix_pp Can not allocate local matrix a_pp: MY_PROC_NUM=",MYPNUM
             STOP
    END IF

    LDA_pp=LDA_A
    A_pp_loc=0.

    step=ntri_p/NPROCS
    ntri_p_loc_b=(rank)*step+1
    ntri_p_loc_e=(rank+1)*step
    if(rank==NPROCS-1) ntri_p_loc_e=ntri_p

do i=ntri_p_loc_b,ntri_p_loc_e
  do k=1,ntri_p
        call get_index_dima(i,i1,ku,ku2)
        dima_sca=0
        dima_sca2=0

        call tri_induct_3_2(ntri_p,i,k,xp,yp,zp,dima_sca)
        call tri_induct_3_2(ntri_p,k,i,xp,yp,zp,dima_sca2)
        dima_sca3=.5*(dima_sca+dima_sca2)

        jdx(i)= jdx(i) + jx(k)*dima_sca3
        jdy(i)= jdy(i) + jy(k)*dima_sca3
        jdz(i)= jdz(i) + jz(k)*dima_sca3
  enddo
enddo

    CALL MPI_ALLREDUCE(jdx, jdx2, ntri_p, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)
    CALL MPI_ALLREDUCE(jdy, jdy2, ntri_p, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)
    CALL MPI_ALLREDUCE(jdz, jdz2, ntri_p, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ier)

    jdx=jdx2
    jdy=jdy2
    jdz=jdz2

do i=1,ntri_p
  do k=1,3
    if (jpot_p(i,k).eq.npot_p) then
      dxp(i,k) = 0.
      dyp(i,k) = 0.
      dzp(i,k) = 0.
      ipot_p(i,k) = 1
    endif
  enddo
enddo


dima_sca3=0.0
do i =1,ntri_p
    do i1=1,ntri_p
        do k =1,3
        j = ipot_p(i,k) + 1
         call  ScaLAPACK_mapping_i(j,i_loc,inside_i)
         if (inside_i) then! If not inside

            do k1=1,3
            j1 = ipot_p(i1,k1) + 1
            call ScaLAPACK_mapping_j(j1,j_loc,inside_j)
            if (inside_j) then! If not inside


                call get_index_dima(i,i1,ku,ku2)

                dima_sca=0
                dima_sca2=0

                call tri_induct_3_2(ntri_p,ku,ku2,xp,yp,zp,dima_sca)
                call tri_induct_3_2(ntri_p,ku2,ku,xp,yp,zp,dima_sca2)
                dima_sca3=.5*(dima_sca+dima_sca2)

                temp  = (dxp(i,k)*dxp(i1,k1)                           &
                        +  dyp(i,k)*dyp(i1,k1)                           &
                        +  dzp(i,k)*dzp(i1,k1)) *dima_sca3

                a_pp_loc(i_loc,j_loc) = a_pp_loc(i_loc,j_loc) + temp

            endif
            enddo
        endif
        enddo
    enddo
enddo


do i=1,ntri_p
    do k=1,3
        j = ipot_p(i,k)
        call ScaLAPACK_mapping_i(1,i_loc,inside_i)
        if (inside_i) then

                    call ScaLAPACK_mapping_j(1+j,j_loc,inside_j)
                    if (inside_j) then

                        temp  = jdx(i)*dxp(i,k)                            &
                               +jdy(i)*dyp(i,k)                            &
                               +jdz(i)*dzp(i,k)

                        a_pp_loc(i_loc,j_loc) = a_pp_loc(i_loc,j_loc) +temp

                    endif
                endif

                call ScaLAPACK_mapping_i(1+j,i_loc,inside_i)
                if (inside_i) then

                    call ScaLAPACK_mapping_j(1,j_loc,inside_j)
                    if (inside_j) then


                        temp  = jdx(i)*dxp(i,k)                            &
                               +jdy(i)*dyp(i,k)                            &
                               +jdz(i)*dzp(i,k)

                        a_pp_loc(i_loc,j_loc) = a_pp_loc(i_loc,j_loc) +temp

                   endif
               endif
        enddo
       enddo


 call ScaLAPACK_mapping_i(1,i_loc,inside_i)
    if (inside_i) then

        call ScaLAPACK_mapping_j(1,j_loc,inside_j)
        if (inside_j) then

            aa = 0.
            do i=1,ntri_p
                aa = aa +jx(i)*jdx(i)+jy(i)*jdy(i)+jz(i)*jdz(i)
            enddo

            a_pp_loc(i_loc,j_loc) = aa

        endif
    endif

  !deallocating all temporary arrays
  deallocate(zm,ym,xm,area,d213,d232,d221,tz3,ty3,tx3,tz2,ty2,tx2, &
             tz1,ty1,tx1,nz,ny,nx)

  deallocate(jdz2,jdy2,jdx2,jdz,jdy,jdx,jz,jy,jx,ipot_p,dzp,dyp,dxp)

 
 if(rank==0) write(*,*) 'matrix pp done'
 if(rank==0) write(*,*) '==============================================================='

end subroutine matrix_pp
