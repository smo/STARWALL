! ----------------------------------------------------------------------
      subroutine  coil_2d(xc,yc,zc,nc,cur,xb,yb,zb,phib)
! ----------------------------------------------------------------------
!
!     purpose:
!
!
! ----------------------------------------------------------------------
      implicit none
      real,dimension (  nc  ,2) :: xc ,yc ,zc
      real,dimension (2*nc-2,3) :: xb, yb ,zb, phib
      real                      :: cur,dist
      integer                   :: j,k,k1,nc
! -------- --------------------------------------------------------------
      dist =  (xc(nc,1)-xc(1,1))**2+(yc(nc,1)-yc(1,1))**2               &
             +(zc(nc,1)-zc(1,1))**2+(xc(nc,2)-xc(1,2))**2               &
             +(yc(nc,2)-yc(1,2))**2+(zc(nc,2)-zc(1,2))**2
      if(dist.gt.1.e-6) then
              write(6,*) dist, xc(nc,1)-xc(1,1),yc(nc,1)-yc(1,1)  &
                         ,zc(nc,1)-zc(1,1)    
              write(6,*)
              write(6,*) dist, xc(nc,2)-xc(1,2),yc(nc,2)-yc(1,2)  &
                        ,zc(nc,2)-zc(1,2)    
              write(6,*) 'coil_2d is not closed:  stop'
              stop
      endif
         j=0
      do k = 1, nc-1
               k1 = k+1
                j = j+1
          xb(j,1) = xc(k ,2)
          yb(j,1) = yc(k ,2)
          zb(j,1) = zc(k ,2)
          xb(j,2) = xc(k1,2)
          yb(j,2) = yc(k1,2)
          zb(j,2) = zc(k1,2)
          xb(j,3) = xc(k ,1)
          yb(j,3) = yc(k ,1)
          zb(j,3) = zc(k ,1)
         phib(j,1)= cur 
         phib(j,2)= cur 
         phib(j,3)=  0.
                 j=j+1
          xb(j,1) = xc(k1,1)
          yb(j,1) = yc(k1,1)
          zb(j,1) = zc(k1,1)
          xb(j,2) = xc(k ,1)
          yb(j,2) = yc(k ,1)
          zb(j,2) = zc(k ,1)
          xb(j,3) = xc(k1,2)
          yb(j,3) = yc(k1,2)
          zb(j,3) = zc(k1,2)
         phib(j,1)= 0.
         phib(j,2)= 0. 
         phib(j,3)= cur
      enddo
 end subroutine  coil_2d
