!DIR$ ATTRIBUTES FORCEINLINE :: ScaLAPACK_mapping_i
subroutine ScaLAPACK_mapping_i(i,i_loc,inside_row)
! ----------------------------------------------------------------------
!   purpose:                                                  20/08/2015
!   Check the mapping "i" index for ScaLAPCK local matrix
!! ----------------------------------------------------------------------

     use sca ! new module for ScaLAPACK variables
! ----------------------------------------------------------------------
      implicit none

      integer  INDXG2L,INDXG2P
      EXTERNAL INDXG2L,INDXG2P

      integer i,i_loc
      logical inside_row

      inside_row = .false.
      ipr = INDXG2P(i,NB,0,0,NPROW)
      if (ipr .eq. MYROW) then
          inside_row = .true.
          i_loc = INDXG2L(i,NB,0,0,NPROW)
      endif

end subroutine ScaLAPACK_mapping_i


!DIR$ ATTRIBUTES FORCEINLINE :: ScaLAPACK_mapping_j
subroutine ScaLAPACK_mapping_j(j,j_loc,inside_col)
! ----------------------------------------------------------------------
!   purpose:                                                  20/08/2015
!   Check the mapping "j" index for ScaLAPCK local matrix
!! ----------------------------------------------------------------------

    use sca ! new module for ScaLAPACK variables
! ----------------------------------------------------------------------
      implicit none


      integer  INDXG2L,INDXG2P
      EXTERNAL INDXG2L,INDXG2P

      integer j,j_loc
      logical inside_col

      inside_col = .false.

      ipc = INDXG2P(j,NB,0,0,NPCOL)
      if (ipc.eq.MYCOL) then

             inside_col = .true.
             j_loc = INDXG2L(j,NB,0,0,NPCOL)

        endif

end subroutine ScaLAPACK_mapping_j


!DIR$ ATTRIBUTES FORCEINLINE :: ScaLAPACK_mapping_i_2
subroutine ScaLAPACK_mapping_i_2(i,i_loc,proc_row)
! ----------------------------------------------------------------------
!   purpose:                                                  20/08/2015
!   Check the mapping "i" index for ScaLAPCK local matrix
!! ----------------------------------------------------------------------

     use sca ! new module for ScaLAPACK variables
! ----------------------------------------------------------------------
      implicit none

      integer  INDXG2L,INDXG2P
      EXTERNAL INDXG2L,INDXG2P

      integer i,i_loc,proc_row
      
     
      proc_row = INDXG2P(i,NB,0,0,NPROW)
      if (proc_row .eq. MYROW)  i_loc = INDXG2L(i,NB,0,0,NPROW)
      

end subroutine ScaLAPACK_mapping_i_2

!DIR$ ATTRIBUTES FORCEINLINE :: ScaLAPACK_mapping_j_2
subroutine ScaLAPACK_mapping_j_2(j,j_loc,proc_col)
! ----------------------------------------------------------------------
!   purpose:                                                  20/08/2015
!   Check the mapping "j" index for ScaLAPCK local matrix
!! ----------------------------------------------------------------------

    use sca ! new module for ScaLAPACK variables
! ----------------------------------------------------------------------
      implicit none


      integer  INDXG2L,INDXG2P
      EXTERNAL INDXG2L,INDXG2P

      integer j,j_loc, proc_col
      
      proc_col = INDXG2P(j,NB,0,0,NPCOL)
      if (proc_col .eq. MYCOL) j_loc = INDXG2L(j,NB,0,0,NPCOL)

end subroutine ScaLAPACK_mapping_j_2



