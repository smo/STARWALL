module coil2d

   implicit none
   integer                            :: ntri_c,ncoil
   integer,dimension(:  ),allocatable :: jtri_c
   real   ,dimension(:,:),allocatable :: x_coil,y_coil,z_coil,phi_coil

end module coil2d
