!----------------------------------------------------------------------
subroutine bfield_par_par(b_par, xp, yp, zp, xu, yu, zu, n, n_b, n_e, x, y, z, ntri, j_pot)
! ----------------------------------------------------------------------
!                                                         23.09.09   
!     purpose:
!
! ----------------------------------------------------------------------
implicit none
integer                     :: n,n_b,n_e,ntri

real,   dimension (n)       :: xp,yp,zp,xu,yu,zu
real,   dimension (n,n_b:n_e) :: b_par
real,   dimension (ntri,3)  :: x,y,z
integer,dimension (ntri,3)  :: j_pot
  
! ----------------------------------------------------------------------
!  local  variables
real              :: x1,y1,z1,x2,y2,z2,x3,y3,z3,sn
real              :: s1,s2,s3,d221,d232,d213,al1,al2,al3         &
                    ,ata1,ata2,ata3,at                           &
                    ,s21,s22,s23,dp1,dm1,dp2,dm2,dp3,dm3         &
                    ,ap1,am1,ap2,am2,ap3,am3                     &
                    ,h,ar1,ar2,ar3,vx,vy,vz                      & 
                    ,x21,y21,z21,x32,y32,z32,x13,y13,z13         &
                    ,tx1,ty1,tz1,tx2,ty2,tz2,tx3,ty3,tz3         &
                    ,nx,ny,nz,pi41,area,d21,d32,d13              &
                    ,jx1,jy1,jz1,jx2,jy2,jz2,jx3,jy3,jz3,temp3   &
                    ,dep1,dep2,dep3,dem1,dem2,dem3,temp1,temp2
integer           :: i,k,j1,j2,j3

do k=1,ntri

  x21   = x(k,2) - x(k,1) 
  y21   = y(k,2) - y(k,1) 
  z21   = z(k,2) - z(k,1) 
  x32   = x(k,3) - x(k,2) 
  y32   = y(k,3) - y(k,2) 
  z32   = z(k,3) - z(k,2) 
  x13   = x(k,1) - x(k,3) 
  y13   = y(k,1) - y(k,3) 
  z13   = z(k,1) - z(k,3)

  d221   =x21**2+y21**2+z21**2
  d232   =x32**2+y32**2+z32**2
  d213   =x13**2+y13**2+z13**2
  d21   = sqrt(d221)
  d32   = sqrt(d232)
  d13   = sqrt(d213)

  nx    = -y21*z13 + z21*y13
  ny    = -z21*x13 + x21*z13
  nz    = -x21*y13 + y21*x13

  area  = 1./sqrt(nx*nx+ny*ny+nz*nz)
  pi41   = .125/asin(1.)
  jx1 = x32*area*pi41
  jy1 = y32*area*pi41
  jz1 = z32*area*pi41

  jx2 = x13*area*pi41
  jy2 = y13*area*pi41
  jz2 = z13*area*pi41

  jx3 = x21*area*pi41
  jy3 = y21*area*pi41
  jz3 = z21*area*pi41

  nx    = nx*area
  ny    = ny*area
  nz    = nz*area
  tx1   = (y32*nz-z32*ny)
  ty1   = (z32*nx-x32*nz)
  tz1   = (x32*ny-y32*nx)
  tx2   = (y13*nz-z13*ny)
  ty2   = (z13*nx-x13*nz)
  tz2   = (x13*ny-y13*nx)
  tx3   = (y21*nz-z21*ny)
  ty3   = (z21*nx-x21*nz)
  tz3   = (x21*ny-y21*nx)
  j1    = j_pot(k,1)
  j2    = j_pot(k,2)
  j3    = j_pot(k,3)


  if(  (j1>=n_b .AND. j1<=n_e) .OR.  (j2>=n_b .AND. j2<=n_e) .OR. (j3>=n_b .AND. j3<=n_e)   ) then

  
   do i=1,n

    x1 = x(k,1) - xp(i)
    y1 = y(k,1) - yp(i)
    z1 = z(k,1) - zp(i)
    x2 = x(k,2) - xp(i)
    y2 = y(k,2) - yp(i)
    z2 = z(k,2) - zp(i)
    x3 = x(k,3) - xp(i)
    y3 = y(k,3) - yp(i)
    z3 = z(k,3) - zp(i)

    sn    = nx*x1+ny*y1+nz*z1
    h     = abs(sn)
    s21   = x1**2+y1**2+z1**2
    s22   = x2**2+y2**2+z2**2
    s23   = x3**2+y3**2+z3**2
    s1    = sqrt(s21)
    s2    = sqrt(s22)
    s3    = sqrt(s23)
    al1   = alog((s2+s1+d21)/(s1+s2-d21))
    al2   = alog((s3+s2+d32)/(s3+s2-d32)) 
    al3   = alog((s1+s3+d13)/(s1+s3-d13))
    ar1   = x1*tx3+y1*ty3+z1*tz3
    ar2   = x2*tx1+y2*ty1+z2*tz1
    ar3   = x3*tx2+y3*ty2+z3*tz2
    dp1   = .5*(s22-s21+d221)
    dp2   = .5*(s23-s22+d232)
    dp3   = .5*(s21-s23+d213)
    dm1   = dp1-d221
    dm2   = dp2-d232
    dm3   = dp3-d213
    ap1   = ar1*dp1
    dep1  = ar1**2+h*d221*(h+s2)
    ap2   = ar2*dp2
    dep2  = ar2**2+h*d232*(h+s3)
    ap3   = ar3*dp3
    dep3  = ar3**2+h*d213*(h+s1)
    am1   = ar1*dm1
    dem1  = ar1**2+h*d221*(h+s1)
    am2   = ar2*dm2
    dem2  = ar2**2+h*d232*(h+s2)
    am3   = ar3*dm3
    dem3  = ar3**2+h*d213*(h+s3)
    ata1  = atan2(ap1*dem1-am1*dep1,dep1*dem1+ap1*am1)
    ata2  = atan2(ap2*dem2-am2*dep2,dep2*dem2+ap2*am2)
    ata3  = atan2(ap3*dem3-am3*dep3,dep3*dem3+ap3*am3)
    at    = sign(1.,sn)*(ata1+ata2+ata3)       
    vx    = -nx*at + al1*tx3/d21+al2*tx1/d32+al3*tx2/d13
    vy    = -ny*at + al1*ty3/d21+al2*ty1/d32+al3*ty2/d13
    vz    = -nz*at + al1*tz3/d21+al2*tz1/d32+al3*tz2/d13


 if(  (j1>=n_b .AND. j1<=n_e)   ) then

    temp1 =                    (vy*jz1-vz*jy1)*xu(i)         &
                             + (vz*jx1-vx*jz1)*yu(i)         &
                             + (vx*jy1-vy*jx1)*zu(i)
    b_par(i,j1) = b_par(i,j1)+ temp1
    
  endif


  if(  (j2>=n_b .AND. j2<=n_e)   ) then

    temp2 =                    (vy*jz2-vz*jy2)*xu(i)         &
                             + (vz*jx2-vx*jz2)*yu(i)         &
                             + (vx*jy2-vy*jx2)*zu(i)
     b_par(i,j2) = b_par(i,j2)+ temp2

   endif


  if(  (j3>=n_b .AND. j3<=n_e)   ) then

    temp3 =                    (vy*jz3-vz*jy3)*xu(i)         &
                             + (vz*jx3-vx*jz3)*yu(i)         &
                             + (vx*jy3-vy*jx3)*zu(i)
    b_par(i,j3) = b_par(i,j3)+ temp3

  endif

  enddo
 endif
enddo



end subroutine bfield_par_par 
