subroutine resistive_wall_response

use icontr
use contr_su
use solv 
use tri_w
use coil2d

use sca
use mpi_v
use time
use resistive
!-----------------------------------------------------------------------
implicit none
include "mpif.h"

integer :: i

!-----------------------------------------------------------------------
if(rank==0) write(*,*) 'resistive_wall_response starts'

nd_bez = (nd_harm + nd_harm0)/2*n_dof_bnd
n_w    = npot_w+ncoil

if(rank==0) write(*,*) '     resistive_wall_response nd_harm  :',nd_harm
if(rank==0) write(*,*) '     resistive_wall_response nd_harm0 :',nd_harm0
if(rank==0) write(*,*) '     resistive_wall_response n_bnd    :',n_bnd
if(rank==0) write(*,*) '     resistive_wall_response nd_bez   :',nd_bez


    allocate(gamma(n_w),stat=ier)
    gamma =0.

    call SCA_GRID(n_w,n_w)
    !allocate local matrix
    allocate(S_ww_loc(MP_A,NQ_A), stat=ier)
    IF (IER /= 0) THEN
             WRITE (*,*) "resistive_wall_response, can not allocate local matrix s_ww_loc MYPROC_NUM=",MYPNUM
             STOP
    END IF
    S_ww_loc=0.
    LDA_sww= LDA_A
!======================================================
call simil_trafo(a_ww_loc,a_rw_loc,n_w,gamma,S_ww_loc)
!======================================================
deallocate(a_ww_loc,a_rw_loc)

!=================================================
call a_ye_computing
!=================================================

!=================================================
call a_ey_computing
!=================================================

!=================================================
call d_ee_computing
!=================================================

n_tor_jorek = 0
do i = 1, n_harm
  if ( n_tor(i) == 0 ) then
    n_tor_jorek = n_tor_jorek + 1
    i_tor_jorek(n_tor_jorek) = 1
  else
    n_tor_jorek = n_tor_jorek + 2
    i_tor_jorek(n_tor_jorek-1:n_tor_jorek) = n_tor(i)*2 + (/ 0, 1 /)
  end if
end do

!=================================================
call computing_s_ww_inverse
!=================================================

call MPI_BARRIER(MPI_COMM_WORLD,ier)
time2=MPI_WTIME()

if(rank==0) write(*,*) 'Total wall clock time before output =', (time2-time1)/3600.0, '  hours'
if(rank==0) write(*,*) '======================================================================='

!=================================================
call output
!=================================================


if(rank==0) write(*,*) 'Program FINISH'
if(rank==0) write(*,*) '======================================================================='

call MPI_BARRIER(MPI_COMM_WORLD,ier)
time3=MPI_WTIME()

if(rank==0) write(*,*) 'Total wall clock time including output=', (time3-time1)/3600.0, '  hours'
if(rank==0) write(*,*) '======================================================================='

end subroutine resistive_wall_response
