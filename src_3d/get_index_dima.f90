subroutine get_index_dima(i,k,ku,j)
! This subroutine is used only in subroutine matrix_pp
! in order to avoid storage of matrix dima

use tri_p !ntri_p  
use icontr !nv  
use contr_su !nu 

implicit none
integer :: i,j,k,ku,nu2,kv,c

    nu2 = 2*nu

    !Calculation of the first index i->ku
    do c=2,nv+1
        if(((c-1)*nu2-i )>=0) then
            kv=c-1
            ku=i-(kv-1)*nu2
            exit
        endif
    enddo

    ! Calculation of the second index k->j
    if(nu2*(kv-1)<k) then
        j=k-nu2*(kv-1)
    else
        j=ntri_p+k-nu2*(kv-1)
    endif

end subroutine get_index_dima
