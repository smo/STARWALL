! ----------------------------------------------------------------------
      subroutine bfield_c(xp,yp,zp,bx,by,bz,n,x,y,z,phi,ntri)
! ----------------------------------------------------------------------
!                                                         27.06.01   
!     purpose:
!
!
! ----------------------------------------------------------------------
      implicit none
      real,dimension (n)       :: xp,yp,zp,bx,by,bz
      real,dimension (ntri,3)  :: x,y,z,phi
      integer                  :: n,ntri
! ----------------------------------------------------------------------
!  local  variables
      real,dimension (n):: x1,y1,z1,x2,y2,z2,x3,y3,z3,sn
      real              :: s1,s2,s3                                    &
                          ,d221,d232,d213,al1,al2,al3                  &
                          ,ata1,ata2,ata3,at                           &
                          ,s21,s22,s23,dp1,dm1,dp2,dm2,dp3,dm3         &
                          ,ap1,am1,ap2,am2,ap3,am3                     &
                          ,h,ar1,ar2,ar3                               & 
                          ,x21,y21,z21,x32,y32,z32,x13,y13,z13,vx,vy,vz&
                          ,tx1,ty1,tz1,tx2,ty2,tz2,tx3,ty3,tz3         &
                          ,nx,ny,nz,pi41,area,d21,d32,d13,jx,jy,jz     &
                          ,dep1,dep2,dep3,dem1,dem2,dem3
      integer           :: i,k
! ----------------------------------------------------------------------
         pi41   = .125/asin(1.)
         bx=0.
         by=0.
         bz=0.

       do i=1,n
        do k=1,ntri
         x21   = x(k,2) - x(k,1)
         y21   = y(k,2) - y(k,1)
         z21   = z(k,2) - z(k,1)
         x32   = x(k,3) - x(k,2)
         y32   = y(k,3) - y(k,2)
         z32   = z(k,3) - z(k,2)
         x13   = x(k,1) - x(k,3)
         y13   = y(k,1) - y(k,3)
         z13   = z(k,1) - z(k,3)
         d221  = x21**2+y21**2+z21**2
         d232  = x32**2+y32**2+z32**2
         d213  = x13**2+y13**2+z13**2
         d21   = sqrt(d221)
         d32   = sqrt(d232)
         d13   = sqrt(d213)
         nx    = -y21*z13 + z21*y13
         ny    = -z21*x13 + x21*z13
         nz    = -x21*y13 + y21*x13
         area  = 1./sqrt(nx*nx+ny*ny+nz*nz)

         jx = (x32*phi(k,1)+x13*phi(k,2)+x21*phi(k,3))*area*pi41
         jy = (y32*phi(k,1)+y13*phi(k,2)+y21*phi(k,3))*area*pi41
         jz = (z32*phi(k,1)+z13*phi(k,2)+z21*phi(k,3))*area*pi41

         nx    = nx*area
         ny    = ny*area
         nz    = nz*area
         tx1   = (y32*nz-z32*ny)
         ty1   = (z32*nx-x32*nz)
         tz1   = (x32*ny-y32*nx)
         tx2   = (y13*nz-z13*ny)
         ty2   = (z13*nx-x13*nz)
         tz2   = (x13*ny-y13*nx)
         tx3   = (y21*nz-z21*ny)
         ty3   = (z21*nx-x21*nz)
         tz3   = (x21*ny-y21*nx)
! ----------------------------------------------------------------------
!      do i=1,n
         x1(i) = x(k,1) - xp(i)
         y1(i) = y(k,1) - yp(i)
         z1(i) = z(k,1) - zp(i)
         x2(i) = x(k,2) - xp(i)
         y2(i) = y(k,2) - yp(i)
         z2(i) = z(k,2) - zp(i)
         x3(i) = x(k,3) - xp(i)
         y3(i) = y(k,3) - yp(i)
         z3(i) = z(k,3) - zp(i)
         sn(i) = nx*x1(i)+ny*y1(i)+nz*z1(i)
          h    = abs(sn(i))
         s21   = x1(i)**2+y1(i)**2+z1(i)**2
         s22   = x2(i)**2+y2(i)**2+z2(i)**2
         s23   = x3(i)**2+y3(i)**2+z3(i)**2
         s1    = sqrt(s21)
         s2    = sqrt(s22)
         s3    = sqrt(s23)
         al1   = alog((s2+s1+d21)/(s1+s2-d21))
         al2   = alog((s3+s2+d32)/(s3+s2-d32))
         al3   = alog((s1+s3+d13)/(s1+s3-d13))
         ar1   = x1(i)*tx3+y1(i)*ty3+z1(i)*tz3
         ar2   = x2(i)*tx1+y2(i)*ty1+z2(i)*tz1
         ar3   = x3(i)*tx2+y3(i)*ty2+z3(i)*tz2
         dp1   = .5*(s22-s21+d221)
         dp2   = .5*(s23-s22+d232)
         dp3   = .5*(s21-s23+d213)
         dm1   = dp1-d221
         dm2   = dp2-d232
         dm3   = dp3-d213
         ap1   = ar1*dp1
         dep1  = ar1**2+h*d221*(h+s2)
         ap2   = ar2*dp2
         dep2  = ar2**2+h*d232*(h+s3)
         ap3   = ar3*dp3
         dep3  = ar3**2+h*d213*(h+s1)
         am1   = ar1*dm1
         dem1  = ar1**2+h*d221*(h+s1)
         am2   = ar2*dm2
         dem2  = ar2**2+h*d232*(h+s2)
         am3   = ar3*dm3
         dem3  = ar3**2+h*d213*(h+s3)
         ata1  = atan2(ap1*dem1-am1*dep1,dep1*dem1+ap1*am1)
         ata2  = atan2(ap2*dem2-am2*dep2,dep2*dem2+ap2*am2)
         ata3  = atan2(ap3*dem3-am3*dep3,dep3*dem3+ap3*am3)
         at    = sign(1.,sn(i))*(ata1+ata2+ata3)
         vx    = -nx*at + al1*tx3/d21+al2*tx1/d32+al3*tx2/d13
         vy    = -ny*at + al1*ty3/d21+al2*ty1/d32+al3*ty2/d13
         vz    = -nz*at + al1*tz3/d21+al2*tz1/d32+al3*tz2/d13
         bx(i) = bx(i)+ vy*jz-vz*jy
         by(i) = by(i)+ vz*jx-vx*jz
         bz(i) = bz(i)+ vx*jy-vy*jx
       enddo
      enddo
      end subroutine bfield_c

