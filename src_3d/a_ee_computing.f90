subroutine a_ee_computing
use solv
use tri_p
use mpi_v
use sca

!-----------------------------------------------------------------------
implicit none


if(rank==0) write(*,*) 'a_ee_computing starts, nd_bez=', nd_bez

call SCA_GRID(nd_bez,nd_bez)
allocate(a_ee_loc(MP_A,NQ_A), stat=ier)
IF (IER /= 0) THEN
           WRITE (*,*) "a_ee_computing.f90: Can not allocate local matrix A_ee_loc : MY_PROC_NUM=",MYPNUM
           STOP
END IF
LDA_ee=LDA_A
a_ee_loc = 0.


!a_ep
CALL DESCINIT(DESCA,nd_bez, npot_p, NB, NB, 0, 0, CONTEXT, LDA_ep, INFO_A )
if(INFO_A .NE. 0) then
  write(6,*) "Something is wrong in a_ee_computing.f90 CALL DESCINIT DESCA, INFO_A=",INFO_A
  stop
endif

!a_pwe_s
CALL DESCINIT(DESCB,npot_p, nd_bez, NB, NB, 0, 0, CONTEXT, LDA_pwe_s, INFO_B )
if(INFO_B .NE. 0) then
  write(6,*) "Something is wrong in a_ee_computing.f90 CALL DESCINIT DESCB, INFO_B=",INFO_B
  stop
endif

!a_ee
CALL DESCINIT(DESCC,nd_bez, nd_bez, NB, NB, 0, 0, CONTEXT, LDA_ee, INFO_C )
if(INFO_B .NE. 0) then
  write(6,*) "Something is wrong in a_ee_computing  CALL DESCINIT DESCC, INFO_C=",INFO_C
  stop
endif


CALL PDGEMM('N','N', nd_bez ,nd_bez, npot_p , 1.0D0  , a_ep_loc_sca ,1,1,DESCA, a_pwe_loc_s,1,1,DESCB,&
             1.0D0, a_ee_loc,1 , 1 , DESCC)

if(rank==0) write(*,*) 'a_ee_computing complited'
if(rank==0) write(*,*) '==============================================================='

end subroutine a_ee_computing






















