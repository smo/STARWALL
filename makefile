SHELL = /bin/sh

BASE_DIR = $(shell pwd)
SRC_DIR = $(BASE_DIR)/src_3d

OSTYPE= $(shell uname)
FILES_MK = files.mk

EXEC_MAIN = STARWALL_JOREK
EXECOS_MAIN = $(EXEC_MAIN)_$(OSTYPE)

DEBUG = no

COMPILER = IFORT
OS_MK = ../config.in

OBJ_DIR = $(BASE_DIR)/o_3d/$(SYS)

export BASE_DIR OBJ_DIR SRC_DIR FILES_MK OS_MK DEBUG COMPILER EXECOS_MAIN

TARGETS = o_3d

.PHONY: $(TARGETS)

all: $(TARGETS)

o_3d: makefile
	test -d $(BASE_DIR)/$@/$(SYS) || mkdir -p $(BASE_DIR)/$@/$(SYS)
	$(MAKE) -C $(SRC_DIR) -f $(OS_MK) \
		$(EXECOS_MAIN)

.PHONY: clean distclean

clean:
	-rm -f $(addsuffix /$(SYS)/*.o,$(TARGETS))
	-rm -f $(addsuffix /$(SYS)/*.mod,$(TARGETS))
	-rm -f $(SRC_DIR)/*.lst
	-rm -f $(SRC_DIR)/*.mod

distclean: clean
	-rm -f $(SRC_DIR)/*.~
	-rm -rf $(TARGETS)
